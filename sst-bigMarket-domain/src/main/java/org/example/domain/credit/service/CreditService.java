package org.example.domain.credit.service;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.example.domain.credit.event.CreditAdjustSuccessMessageEvent;
import org.example.domain.credit.model.aggregate.TradeAggregate;
import org.example.domain.credit.model.entity.CreditAccountEntity;
import org.example.domain.credit.model.entity.CreditOrderEntity;
import org.example.domain.credit.model.entity.TradeEntity;
import org.example.domain.credit.model.valobj.TradeNameVO;
import org.example.domain.credit.model.valobj.TradeTypeVO;
import org.example.domain.credit.repository.ICreditRepository;
import org.example.domain.task.model.entity.TaskEntity;
import org.example.domain.task.model.valobj.TaskStateVO;
import org.example.types.event.BaseEvent;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;



@Service
public class CreditService implements ICreditAdjustService {


    @Resource
    private ICreditRepository creditRepository;


    @Resource
    private CreditAdjustSuccessMessageEvent creditAdjustSuccessMessageEvent;

    /**
     * 创建积分调额订单
     * 构建调额额度入账实体和调额订单聚合对象
     * 并录入数据库
     * @param tradeEntity
     * @return
     */
    @Override
    public String createOrder(TradeEntity tradeEntity) {
        TradeNameVO tradeName = tradeEntity.getTradeName();
        TradeTypeVO tradeType = tradeEntity.getTradeType();
        BigDecimal amount = tradeEntity.getAmount();
        String userId = tradeEntity.getUserId();
        String outBusinessNo = tradeEntity.getOutBusinessNo();

        //创建聚合对象

        //1.构建积分订单实体
        CreditOrderEntity creditOrderEntity =  CreditOrderEntity.builder()
                .orderId(RandomStringUtils.randomNumeric(12))
                .tradeAmount(amount)
                .outBusinessNo(outBusinessNo)
                .tradeType(tradeType)
                .tradeName(tradeName)
                .userId(userId)
                .build();

        //2.构建积分调额实体
        CreditAccountEntity creditAccountEntity = CreditAccountEntity.builder()
                .totalAmount(amount)
                .availableAmount(amount)
                .userId(userId)
                .build();

        // 3. 构建消息任务对象
        CreditAdjustSuccessMessageEvent.CreditAdjustSuccessMessage creditAdjustSuccessMessage = new CreditAdjustSuccessMessageEvent.CreditAdjustSuccessMessage();
        creditAdjustSuccessMessage.setUserId(tradeEntity.getUserId());
        creditAdjustSuccessMessage.setOrderId(creditOrderEntity.getOrderId());
        creditAdjustSuccessMessage.setAmount(tradeEntity.getAmount());
        creditAdjustSuccessMessage.setOutBusinessNo(tradeEntity.getOutBusinessNo());
        BaseEvent.EventMessage<CreditAdjustSuccessMessageEvent.CreditAdjustSuccessMessage> creditAdjustSuccessMessageEventMessage = creditAdjustSuccessMessageEvent.buildEventMessage(creditAdjustSuccessMessage);

        TaskEntity taskEntity = TaskEntity.builder()
                .userId(userId)
                .topic(creditAdjustSuccessMessageEvent.topic())
                .messageId(creditAdjustSuccessMessageEventMessage.getId())
                .message(JSON.toJSONString(creditAdjustSuccessMessageEventMessage))
                .state(TaskStateVO.create)
                .build();

        TradeAggregate tradeAggregate = new TradeAggregate();
        tradeAggregate.setCreditOrderEntity(creditOrderEntity);
        tradeAggregate.setCreditAccountEntity(creditAccountEntity);
        tradeAggregate.setTask(taskEntity);


        tradeAggregate.setUserId(userId);

        creditRepository.saveUserCreditTradeOrder(tradeAggregate);

        return tradeAggregate.getCreditOrderEntity().getOrderId();
    }
}
