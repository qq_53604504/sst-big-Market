package org.example.domain.credit.service;

import org.example.domain.credit.model.entity.TradeEntity;

/**
 * 积分调整服务
 */
public interface ICreditAdjustService {

    /**
     * 创建积分调整订单
     * @param tradeEntity
     * @return 订单id
     */
    String createOrder(TradeEntity tradeEntity);

}
