package org.example.domain.task.service;

import org.example.domain.task.model.entity.TaskEntity;
import org.example.domain.task.repository.ITaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskService implements ITaskService{

    @Autowired
    private ITaskRepository taskRepository;

    /**
     * 查询未发送成功的消息列表
     * @return
     */
    @Override
    public List<TaskEntity> queryNoSendMessageTaskList() {
        return taskRepository.queryNoSendMessageTaskList();
    }

    /**
     * 发送消息
     * @param taskEntity 待发送消息实体
     */
    @Override
    public void sendMessage(TaskEntity taskEntity) {
        taskRepository.sendMessage(taskEntity);
    }

    /**
     * 更新任务流水表状态为完成
     * @param userId
     * @param messageId
     */
    @Override
    public void updateTaskSendMessageCompleted(String userId, String messageId) {

        taskRepository.updateTaskSendMessageCompleted(userId,messageId);
    }

    /**
     * 更新任务流水表为失败
     * @param userId
     * @param messageId
     */
    @Override
    public void updateTaskSendMessageFail(String userId, String messageId) {
        taskRepository.updateTaskSendMessageFail(userId,messageId);
    }
}
