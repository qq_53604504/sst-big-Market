package org.example.domain.task.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.domain.task.model.valobj.TaskStateVO;


/**
 * 消息流水表实体
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TaskEntity {

    private String userId;

    private String topic;

    private String messageId;

    //消息格式为JSON
    private String message;

    private TaskStateVO state;
}
