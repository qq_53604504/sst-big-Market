package org.example.domain.task.service;

import org.example.domain.task.model.entity.TaskEntity;

import java.util.List;

/**
 * 消息流水表
 * 负责消息流水相关方法
 * 如流水表记录查询,发送消息等等
 */
public interface ITaskService {
    List<TaskEntity> queryNoSendMessageTaskList();

    void sendMessage(TaskEntity taskEntity);

    void updateTaskSendMessageCompleted(String userId, String messageId);

    void updateTaskSendMessageFail(String userId, String messageId);
}
