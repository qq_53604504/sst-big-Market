package org.example.domain.award.repository;

import org.example.domain.award.model.aggregate.GiveOutPrizesAggregate;
import org.example.domain.award.model.aggregate.UserAwardRecordAggregate;
import org.example.domain.award.model.entity.AwardEntity;

public interface IAwardRepository {


    void saveUserAwardRecord(UserAwardRecordAggregate userAwardRecordAggregate);

    void saveGiveOutPrizesAggregate(GiveOutPrizesAggregate giveOutPrizesAggregate);

    String queryAwardKeyByAwardId(Integer awardId);

    String queryAwardConfigByAwardId(Integer awardId);

}
