package org.example.domain.award.model.aggregate;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.domain.task.model.entity.TaskEntity;
import org.example.domain.award.model.entity.UserAwardRecordEntity;

/**
 * 用户中奖记录聚合对象 【聚合代表一个事务操作】
 * 既写入中奖记录,又写入发送发奖消息的流水记录
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserAwardRecordAggregate {

    private UserAwardRecordEntity userAwardRecordEntity;

    private TaskEntity taskEntity;

}
