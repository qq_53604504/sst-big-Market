package org.example.domain.award.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Award实体
 * 包含奖品配置,奖品标识Key 奖品id
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AwardEntity {

    private Integer awardId;
    private String awardKey;
    private String awardConfig;
    private String awardDesc;
}
