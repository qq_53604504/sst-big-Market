package org.example.domain.award.model.aggregate;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.domain.award.model.entity.UserAwardRecordEntity;
import org.example.domain.award.model.entity.UserCreditAwardEntity;

/**
 * 积分奖品发奖聚合对象
 * 抽奖奖品记录实体
 * 积分奖品实体
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GiveOutPrizesAggregate {

    private UserAwardRecordEntity userAwardRecordEntity;

    private UserCreditAwardEntity userCreditAwardEntity;

    private String userId;
}
