package org.example.domain.award.service.distribute;

import org.example.domain.award.model.entity.DistributeAwardEntity;

/**
 * 发奖有不同的奖品,意味着要根据不同奖品类型进行发奖,常规的做法是根据奖品类型编写if-else
 * 这里使用策略设计模式,将不同类型的奖品发奖逻辑分别编写成方法,使用统一抽象接口,不同实现类来代表不同发奖方式
 */
public interface IDistributeAward {

    void giveOutPrizes(DistributeAwardEntity distributeAwardEntity);
}
