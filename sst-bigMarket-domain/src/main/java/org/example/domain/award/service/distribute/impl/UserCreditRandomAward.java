package org.example.domain.award.service.distribute.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.example.domain.award.model.aggregate.GiveOutPrizesAggregate;
import org.example.domain.award.model.entity.DistributeAwardEntity;
import org.example.domain.award.model.entity.UserAwardRecordEntity;
import org.example.domain.award.model.entity.UserCreditAwardEntity;
import org.example.domain.award.repository.IAwardRepository;
import org.example.domain.award.service.distribute.IDistributeAward;
import org.example.types.common.Constants;
import org.example.types.enums.ResponseCode;
import org.example.types.exception.AppException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * 随机积分发奖实现类
 * 通过@Component("user_credit_random")将该类加入容器管理,在使用时获取
 */
@Slf4j
@Component("user_credit_random")
public class UserCreditRandomAward implements IDistributeAward {

    @Resource
    private IAwardRepository awardRepository;

    /**
     * 积分奖品发奖
     * 根据积分奖品增加用户积分
     * 根据抽奖奖品记录更新其状态从创建到已完成
     * @param distributeAwardEntity
     */
    @Override
    public void giveOutPrizes(DistributeAwardEntity distributeAwardEntity) {
        String userId = distributeAwardEntity.getUserId();
        Integer awardId = distributeAwardEntity.getAwardId();
        String orderId = distributeAwardEntity.getOrderId();
        String awardTitle = distributeAwardEntity.getAwardTitle();
        String config = distributeAwardEntity.getConfig();
        if (StringUtils.isBlank(config)){
            config = awardRepository.queryAwardConfigByAwardId(awardId);
        }


        String[] creditRange = config.split(Constants.SPLIT);
        if ( creditRange.length != 2){

            log.error("积分值配置有问题");
            throw new AppException(ResponseCode.AWARD_CONFIG_ERROR.getCode(),ResponseCode.AWARD_CONFIG_ERROR.getInfo());
        }

        BigDecimal creditAmount = generateRandomCreditAmount(creditRange[0],creditRange[1]);

        //构建聚合对象
        GiveOutPrizesAggregate giveOutPrizesAggregate = new GiveOutPrizesAggregate();
        giveOutPrizesAggregate.setUserCreditAwardEntity(UserCreditAwardEntity.builder()
                        .creditAmount(creditAmount)
                        .userId(userId)
                .build());
        giveOutPrizesAggregate.setUserAwardRecordEntity(UserAwardRecordEntity.builder()
                        .userId(userId)
                        .awardId(awardId)
                        .orderId(orderId)
                .build());
        giveOutPrizesAggregate.setUserId(userId);

        //操作数据库
        awardRepository.saveGiveOutPrizesAggregate(giveOutPrizesAggregate);

    }

    /**
     * 根据生成一个介于最大值和最小值之间的随机值,当作积分
     * @param min
     * @param max
     * @return
     */
    private BigDecimal generateRandomCreditAmount(String min, String max) {
        BigDecimal minBigDecimal = new BigDecimal(min);
        BigDecimal maxBigDecimal = new BigDecimal(max);

        BigDecimal randomCreditAmount = minBigDecimal.add(BigDecimal.valueOf(Math.random()).multiply(maxBigDecimal.subtract(minBigDecimal)));

        return randomCreditAmount;
    }
}
