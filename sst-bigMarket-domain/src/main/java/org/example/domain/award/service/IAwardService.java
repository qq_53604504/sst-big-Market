package org.example.domain.award.service;

import org.example.domain.award.model.entity.DistributeAwardEntity;
import org.example.domain.award.model.entity.UserAwardRecordEntity;

/**
 * 中奖奖品接口
 * 负责抽到相应奖品之后的各种操作
 * 包括写入抽奖记录,分发奖品等等
 */
public interface IAwardService {
    void saveUserAwardRecord(UserAwardRecordEntity userAwardRecordEntity);

    /**
     * 消费者接受发奖消息的回调函数
     */
    void distributeAward(DistributeAwardEntity distributeAwardEntity);
}
