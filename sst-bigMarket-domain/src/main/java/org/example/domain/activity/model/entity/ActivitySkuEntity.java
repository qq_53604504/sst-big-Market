package org.example.domain.activity.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ActivitySkuEntity {

    /**
     * 商品sku
     */
    private Long sku;

    private Long activityId;

    private Long activityCountId;

    /** 库存总量 */
    private Long stockCount;
    /** 剩余库存 */
    private Long stockCountSurplus;

}
