package org.example.domain.activity.service;

import org.example.domain.activity.model.entity.PartakeRaffleActivityEntity;
import org.example.domain.activity.model.entity.UserRaffleOrderEntity;

/**
 * 用户参与活动接口
 * 定义用户参与活动抽奖的相关方法
 */
public interface IActivityPartakeService {

    /**
     * 用户参与抽奖活动:扣减活动账户库存，产生抽奖单。
     * 如存在未被使用的抽奖单则直接返回已存在的抽奖单。
     * @param partakeRaffleActivityEntity 参与抽奖活动实体对象:用户id,活动id
     * @return 用户抽奖订单实体对象
     */
    UserRaffleOrderEntity createOrder(PartakeRaffleActivityEntity partakeRaffleActivityEntity);

}