package org.example.domain.activity.service.quota;

import org.apache.commons.lang3.RandomStringUtils;
import org.example.domain.activity.model.aggregate.CreateOrderAggregate;
import org.example.domain.activity.model.entity.*;
import org.example.domain.activity.model.valobj.OrderStateVO;
import org.example.domain.activity.model.valobj.SubtractionSkuStockMessageVO;
import org.example.domain.activity.repository.IActivityRepository;
import org.example.domain.activity.service.IActivitySkuStock;
import org.example.domain.activity.service.quota.policy.ITradePolicy;
import org.example.domain.activity.service.quota.rule.chain.Factory.DefaultActivityChainFactory;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

/**
 *
 */
@Service
public class ActivityAccountQuotaService extends AbstractActivityAccountQuotaService implements IActivitySkuStock {


    public ActivityAccountQuotaService(IActivityRepository raffleActivityRepository, DefaultActivityChainFactory defaultActivityChainFactory, Map<String, ITradePolicy> tradePolicyGroup) {
        super(raffleActivityRepository, defaultActivityChainFactory, tradePolicyGroup);
    }

    @Override
    protected void doSaveOrder(CreateOrderAggregate createOrderAggregate) {

        activityRepository.doSaveOrder(createOrderAggregate);


    }

    /**
     * 积分兑换sku出货
     * 更新订单状态
     * sku商品入账:活动额度增加
     * @param deliveryOrderEntity 积分兑换sku出货单实体对象
     */
    @Override
    public void updateOrder(DeliveryOrderEntity deliveryOrderEntity) {
        activityRepository.updateOrder(deliveryOrderEntity);
    }

    /**
     * 构建聚合对象
     * 活动sku订单实体(状态设置已完成) , 抽奖额度调整信息
     * @param skuRechargeEntity
     * @param activitySkuEntity
     * @param activityEntity
     * @param activityCountEntity
     * @return
     */
    @Override
    protected CreateOrderAggregate buildCreateOrderAggregate(SkuRechargeEntity skuRechargeEntity, ActivitySkuEntity activitySkuEntity, ActivityEntity activityEntity, ActivityCountEntity activityCountEntity) {
        ActivityOrderEntity activityOrderEntity = ActivityOrderEntity.builder()
                .userId(skuRechargeEntity.getUserId())
                .sku(skuRechargeEntity.getSku())
                .outBusinessNo(skuRechargeEntity.getOutBusinessNo())
                .totalCount(activityCountEntity.getTotalCount())
                .dayCount(activityCountEntity.getDayCount())
                .monthCount(activityCountEntity.getMonthCount())
                .activityId(activityEntity.getActivityId())
                .activityName(activityEntity.getActivityName())
                .strategyId(activityEntity.getStrategyId())
                .state(OrderStateVO.completed)
                .orderId(RandomStringUtils.randomNumeric(12))//随机生成orderID
                .orderTime(new Date())
                .activityId(activitySkuEntity.getActivityId())
                .build();



        CreateOrderAggregate createOrderAggregate = CreateOrderAggregate.builder()
                .totalCount(activityCountEntity.getTotalCount())
                .dayCount(activityCountEntity.getDayCount())
                .monthCount(activityCountEntity.getMonthCount())
                .activityId(activityEntity.getActivityId())
                .activityId(activitySkuEntity.getActivityId())
                .userId(skuRechargeEntity.getUserId())
                .activityOrderEntity(activityOrderEntity)
                .build();

        return createOrderAggregate;


    }

    @Override
    public SubtractionSkuStockMessageVO takeQueueValue(String activitySkuCountQueryKey) {


        return activityRepository.takeQueueValue(activitySkuCountQueryKey);
    }

    @Override
    public boolean subtractionSkuStock(SubtractionSkuStockMessageVO subtractionSkuStockMessageVO) {
        return  activityRepository.subtractionSkuStock(subtractionSkuStockMessageVO);

    }

    @Override
    public void clearActivitySkuStock(Long sku) {
        activityRepository.clearActivitySkuStock(sku);
    }

    @Override
    public void clearQueueValue(Long sku) {
        activityRepository.clearQueueValue(sku);
    }


    /**
     * 查询用户某天的抽奖日额度数据
     * @param activityId 活动id
     * @param userId 用户id
     * @param day 天
     * @return
     */
    @Override
    public ActivityAccountDayEntity queryRaffleActivityAccountDay(Long activityId, String userId, String day) {
        ActivityAccountDayEntity activityAccountDayEntity =  activityRepository.queryRaffleActivityAccountDay(activityId,userId,day);
        return activityAccountDayEntity;
    }

    @Override
    public ActivityAccountEntity queryUserActivityAccount(Long activityId, String userId) {
        return activityRepository.queryUserActivityAccount(activityId,userId);
    }

    @Override
    public Integer queryRaffleActivityAccountPartakeCount(Long activityId, String userId) {

        return activityRepository.queryActivityAccountTotalUseCountByActivityId(userId, activityId);
    }
}
