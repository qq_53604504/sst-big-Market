package org.example.domain.activity.service.armory;

public interface IActivityArmory {


    /**
     * 根据sku装配活动库存
     * @param sku
     */
    void assembleActivitySku(Long sku);

    /**
     * 根据活动id装配活动sku库存
     * sku: activity_count和activity的组合 ,表示活动库存
     * activity_count表:每次活动所能增加的用户额度
     * @param activityId 活动id
     */
    void assembleActivitySkuByActivityId(Long activityId);
}
