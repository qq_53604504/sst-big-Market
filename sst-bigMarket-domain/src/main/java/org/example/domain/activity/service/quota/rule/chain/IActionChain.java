package org.example.domain.activity.service.quota.rule.chain;

import org.example.domain.activity.model.entity.ActivityCountEntity;
import org.example.domain.activity.model.entity.ActivityEntity;
import org.example.domain.activity.model.entity.ActivitySkuEntity;

/**
 * 责任链节点抽象接口 ,和链表结构体很像,在
 * 包含两个方法:
 *  1.添加下一个节点的方法appendNext(),这个常规链表结构中没有这个方法,是因为这里想在添加下一个节点时同时返回下一个节点,以此来做到链式添加.
 *  2.实际运行的方法action(),责任链实际执行逻辑的方法
 */
public interface IActionChain {

    /**
     * 为当前节点添加下一个节点,并且返回下一个节点,做到链式添加
     * @param next 待添加的下一个节点
     * @return 下一个节点
     */
    IActionChain appendNext(IActionChain next );

    /**
     * 执行逻辑的方法
     * @param activitySkuEntity
     * @param activityEntity
     * @param activityCountEntity
     * @return
     */
    Boolean action(ActivitySkuEntity activitySkuEntity, ActivityEntity activityEntity, ActivityCountEntity activityCountEntity,String userId);






}
