package org.example.domain.activity.service.partake;

import lombok.extern.slf4j.Slf4j;
import org.example.domain.activity.model.entity.ActivityAccountDayEntity;
import org.example.domain.activity.model.entity.ActivityAccountEntity;
import org.example.domain.activity.model.entity.ActivityAccountMonthEntity;
import org.example.domain.activity.model.entity.ActivityEntity;
import org.example.domain.activity.model.valobj.ActivityStateVO;
import org.example.domain.activity.repository.IActivityRepository;
import org.example.types.common.Constants;
import org.example.types.enums.ResponseCode;
import org.example.types.exception.AppException;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 参与活动支持类
 * 用于解耦业务逻辑和具体的数据操作
 * 提供一些通用的、重复使用的功能,如封装公共查询
 */
@Slf4j
public class ActivityPartakeSupport {

    protected IActivityRepository activityRepository;

    public ActivityPartakeSupport(IActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    //获取当前时间
    protected final Date currentDate = new Date();

    /**
     * 检查活动状态
     * @param activityEntity 活动实体
     */
    public void checkActivityState(ActivityEntity activityEntity) {
        if (currentDate.before(activityEntity.getBeginDateTime()) && currentDate.after(activityEntity.getEndDateTime())) {
            log.info("时间不符合");
            throw new AppException(ResponseCode.ACTIVITY_DATE_ERROR.getCode(), ResponseCode.ACTIVITY_DATE_ERROR.getInfo());
        }
        if (!activityEntity.getState().equals(ActivityStateVO.open)) {
            log.info("活动未开启");
            throw new AppException(ResponseCode.ACTIVITY_STATE_ERROR.getCode(), ResponseCode.ACTIVITY_STATE_ERROR.getInfo());
        }
    }

    /**
     * 检查该用户是否存在日账户数据和月账户数据,如果不存在则同步总账户数据
     *
     * @param userId
     * @param activityId
     */
    public void checkMonthDayIsExist(String userId, Long activityId,Date currentDate) {
        ActivityAccountEntity activityAccountEntity = activityRepository.queryActivityAccountEntityByUserId(userId, activityId);

        if (activityAccountEntity == null || activityAccountEntity.getTotalCountSurplus() < 0) {
            log.info("活动库存不足");
            throw new AppException(ResponseCode.ACCOUNT_QUOTA_ERROR.getCode(), ResponseCode.ACCOUNT_QUOTA_ERROR.getInfo());
        }
        String day = Constants.dateFormatDay.format(currentDate);
        String month =Constants.dateFormatMonth.format(currentDate);
        //检查日额度记录
        ActivityAccountDayEntity activityAccountDayEntity = activityRepository.queryActivityAccountDayEntityByUserIdAndActivityId(userId, activityId, day);

        if (activityAccountDayEntity != null && activityAccountDayEntity.getDayCountSurplus() < 0) {
            log.info("日活动库存不足");
            throw new AppException(ResponseCode.ACCOUNT_DAY_QUOTA_ERROR.getCode(), ResponseCode.ACCOUNT_DAY_QUOTA_ERROR.getInfo());
        } else if (activityAccountDayEntity == null) {

            activityRepository.saveActivityAccountDay(ActivityAccountDayEntity.builder()
                    .userId(activityAccountEntity.getUserId())
                    .activityId(activityAccountEntity.getActivityId())
                    .day(day)
                    .dayCount(activityAccountEntity.getDayCount())
                    .dayCountSurplus(activityAccountEntity.getDayCountSurplus())
                    .build());
        }

        //检查月额度记录
        ActivityAccountMonthEntity activityAccountMonthEntity = activityRepository.queryActivityAccountMonthEntityByUserId(userId, activityId, month);
        if (activityAccountMonthEntity != null && activityAccountMonthEntity.getMonthCountSurplus() < 0) {
            log.info("月活动库存不足");
            throw new AppException(ResponseCode.ACCOUNT_MONTH_QUOTA_ERROR.getCode(), ResponseCode.ACCOUNT_MONTH_QUOTA_ERROR.getInfo());
        } else if (activityAccountMonthEntity == null) {

            activityRepository.saveActivityAccountMonth(ActivityAccountMonthEntity.builder()
                    .userId(activityAccountEntity.getUserId())
                    .activityId(activityAccountEntity.getActivityId())
                    .month(month)
                    .monthCount(activityAccountEntity.getMonthCount())
                    .monthCountSurplus(activityAccountEntity.getMonthCountSurplus())
                    .build());
        }
    }
}
