package org.example.domain.activity.service.quota.rule.chain.impl;

import lombok.extern.slf4j.Slf4j;
import org.example.domain.activity.model.entity.*;
import org.example.domain.activity.model.valobj.SubtractionSkuStockMessageVO;
import org.example.domain.activity.repository.IActivityRepository;
import org.example.domain.activity.service.quota.rule.chain.AbstractActionChain;
import org.example.types.common.Constants;
import org.example.types.enums.ResponseCode;
import org.example.types.exception.AppException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 责任链单个节点: 用户额度账户检查
 */
@Slf4j
@Component("activity_account_exist_action")
public class ActivityAccountExistActionChain extends AbstractActionChain {


    @Resource
    private IActivityRepository  activityRepository;

    /**
     * 检查用户额度账户是否存在,总账户不存在则报错,月日账户不存在则同步总账户
     * @param activitySkuEntity  活动sku实体
     * @param activityEntity 活动实体
     * @param activityCountEntity 一次活动账户增长次数实体
     * @param userId 用户id
     * @return
     */
    @Override
    public Boolean action(ActivitySkuEntity activitySkuEntity, ActivityEntity activityEntity, ActivityCountEntity activityCountEntity,String userId) {
        log.info("活动责任链-检查用户账户是否存在 开始。");
        checkMonthDayIsExist(userId,activityEntity.getActivityId(),new Date());
        return true;
    }

    /**
     * 检查该用户是否存在日账户数据和月账户数据,如果不存在则同步总账户数据
     *
     * @param userId
     * @param activityId
     * @param currentDate 日期
     */
    public void checkMonthDayIsExist(String userId, Long activityId, Date currentDate) {
        activityRepository.checkMonthDayIsExist(userId, activityId, currentDate);
    }
}
