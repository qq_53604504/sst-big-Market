package org.example.domain.activity.service.armory;

import org.example.domain.activity.model.entity.ActivitySkuEntity;
import org.example.domain.activity.repository.IActivityRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class ActivityDispatchArmory implements IActivityArmory,IActivityDispatch {


    @Resource
    private IActivityRepository raffleActivityRepository;


    /**
     * 把库存从数据库中取出来放进缓存
     * @param sku
     */
    @Override
    public void assembleActivitySku(Long sku) {
        ActivitySkuEntity activitySkuEntity = raffleActivityRepository.queryActivitySkuEntityBySku(sku);
        raffleActivityRepository.assembleSkuStockInRedis(activitySkuEntity);

    }


    /**
     * 根据活动id装配活动sku库存
     * sku: activity_count和activity的组合 ,表示活动库存
     * activity_count表:每次活动所能增加的用户额度
     * @param activityId 活动id
     */
    @Override
    public void assembleActivitySkuByActivityId(Long activityId) {
        List<ActivitySkuEntity> activitySkuEntities = raffleActivityRepository.queryActivitySkuEntitiesByActivityId(activityId);
        for (ActivitySkuEntity activitySkuEntity : activitySkuEntities) {
            raffleActivityRepository.assembleSkuStockInRedis(activitySkuEntity);
        }
    }

    /**
     * 根据sku在redis中扣减库存
     * @param sku
     * @param endTime 活动截至时间, 根据这个设置redis里sku库存的过期时间,因为活动过期这个redis库存也就没用了
     * @return
     */
    @Override
    public Boolean subtractionSkuStockFromRedis(Long sku, Date endTime) {
        return raffleActivityRepository.subtractionSkuStockFromRedis(sku,endTime);
    }


}
