package org.example.domain.activity.service.armory;

import java.util.Date;

public interface IActivityDispatch {


    /**
     * 根据sku在redis中扣减库存 ,
     * @param sku
     * @param endTime 活动截至时间, 根据这个设置redis里sku库存的流水锁的过期时间,因为活动过期这个锁也就没用了
     * @return
     */
    Boolean subtractionSkuStockFromRedis(Long sku, Date endTime);
}
