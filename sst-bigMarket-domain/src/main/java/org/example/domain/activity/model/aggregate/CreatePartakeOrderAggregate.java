package org.example.domain.activity.model.aggregate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.domain.activity.model.entity.ActivityAccountDayEntity;
import org.example.domain.activity.model.entity.ActivityAccountEntity;
import org.example.domain.activity.model.entity.ActivityAccountMonthEntity;
import org.example.domain.activity.model.entity.UserRaffleOrderEntity;

/**
 *
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreatePartakeOrderAggregate {


    private UserRaffleOrderEntity userRaffleOrderEntity;


}
