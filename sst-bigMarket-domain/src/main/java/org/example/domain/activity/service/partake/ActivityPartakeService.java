package org.example.domain.activity.service.partake;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.example.domain.activity.model.aggregate.CreatePartakeOrderAggregate;
import org.example.domain.activity.model.entity.*;
import org.example.domain.activity.model.valobj.UserRaffleOrderStateVO;
import org.example.domain.activity.repository.IActivityRepository;
import org.example.types.enums.ResponseCode;
import org.example.types.exception.AppException;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

import static java.time.LocalTime.now;

/**
 * 实现类,实现具体细节
 */
@Slf4j
@Service
public class ActivityPartakeService extends AbstractActivityPartake {

    public ActivityPartakeService(IActivityRepository activityRepository) {
        super(activityRepository);
    }

    /**
     * 构建用户抽奖订单
     * 设置状态为创建,中奖后写入中奖记录时更新为完成
     * @param userId 用户id
     * @param activityEntity 活动实体
     * @return
     */
    @Override
    protected UserRaffleOrderEntity buildUserRaffleOrderEntity(String userId, ActivityEntity activityEntity) {

        UserRaffleOrderEntity userRaffleOrderEntity = UserRaffleOrderEntity.builder()
                .activityId(activityEntity.getActivityId())
                .activityName(activityEntity.getActivityName())
                .strategyId(activityEntity.getStrategyId())
                .orderId(RandomStringUtils.randomNumeric(12))
                .orderState(UserRaffleOrderStateVO.create)
                .orderTime(new Date())
                .userId(userId)
                .endDateTime(activityEntity.getEndDateTime())
                .build();

        return userRaffleOrderEntity;
    }



}
