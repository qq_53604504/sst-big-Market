package org.example.domain.activity.service.quota.rule.chain.impl;

import lombok.extern.slf4j.Slf4j;
import org.example.domain.activity.model.entity.ActivityCountEntity;
import org.example.domain.activity.model.entity.ActivityEntity;
import org.example.domain.activity.model.entity.ActivitySkuEntity;
import org.example.domain.activity.model.valobj.ActivityStateVO;
import org.example.domain.activity.service.quota.rule.chain.AbstractActionChain;
import org.example.types.enums.ResponseCode;
import org.example.types.exception.AppException;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 责任链单个节点: 规则校验
 * 1.活动时间
 * 2.活动状态
 * 3.sku库存
 */
@Slf4j
@Component("activity_base_action")
public class ActivityBaseActionChain extends AbstractActionChain {




    @Override
    public Boolean action(ActivitySkuEntity activitySkuEntity, ActivityEntity activityEntity, ActivityCountEntity activityCountEntity,String userId) {
        log.info("活动责任链-基础信息【有效期、状态】校验开始。");

        Date date = new Date();

        if (activityEntity.getBeginDateTime().after(date) || activityEntity.getEndDateTime().before(date)){
            log.info("未到活动开放时间");
            throw new AppException(ResponseCode.ACTIVITY_DATE_ERROR.getCode(),ResponseCode.ACTIVITY_DATE_ERROR.getInfo());
        }
        if (!activityEntity.getState().equals(ActivityStateVO.open)){
            log.info("活动未开启");
            throw new AppException(ResponseCode.ACTIVITY_STATE_ERROR.getCode(),ResponseCode.ACTIVITY_STATE_ERROR.getInfo());

        }
        if (activitySkuEntity.getStockCountSurplus()<=0){
            log.info("库存不足");
            throw new AppException(ResponseCode.ACTIVITY_SKU_STOCK_ERROR.getCode(),ResponseCode.ACTIVITY_SKU_STOCK_ERROR.getInfo());

        }
        return next.action(activitySkuEntity,activityEntity,activityCountEntity,userId);
    }
}
