package org.example.domain.activity.repository;

import org.example.domain.activity.model.aggregate.CreateOrderAggregate;
import org.example.domain.activity.model.aggregate.CreatePartakeOrderAggregate;
import org.example.domain.activity.model.entity.*;
import org.example.domain.activity.model.valobj.SubtractionSkuStockMessageVO;

import java.util.Date;
import java.util.List;


public interface IActivityRepository {


    ActivityCountEntity queryActivityCountEntityById(Long raffleActivityCountId);

    ActivityEntity queryActivityEntityByActivityId(Long activityId);

    ActivitySkuEntity queryActivitySkuEntityBySku(Long sku);

    ActivityAccountEntity queryActivityAccountEntityByUserId(String userId,Long activityId);

    public List<ActivityOrderEntity> queryActivityOrderEntityListByUserId(String userId);


    void doSaveOrder(CreateOrderAggregate createOrderAggregate);

    void assembleSkuStockInRedis(ActivitySkuEntity activitySkuEntity);

    Boolean subtractionSkuStockFromRedis(Long sk, Date endDateTime);


    void sendSubtractionSkuStockMessage(SubtractionSkuStockMessageVO build,Long sku);

    Boolean subtractionSkuStock(SubtractionSkuStockMessageVO subtractionSkuStockMessageVO);

    SubtractionSkuStockMessageVO takeQueueValue(String activitySkuCountQueryKey);

    void clearActivitySkuStock(Long sku);

    void clearQueueValue(Long sku);

    ActivityAccountDayEntity queryActivityAccountDayEntityByUserIdAndActivityId(String userId, Long activityId,String day);

    ActivityAccountMonthEntity queryActivityAccountMonthEntityByUserId(String userId,Long activityId,String month);


    UserRaffleOrderEntity queryNoUsedRaffleOrder(PartakeRaffleActivityEntity partakeRaffleActivityEntity);

    /**
     *  根据活动id查询活动sku实体
     *  一个活动可能有多个活动sku
     * @param activityId 活动id
     * @return 活动实体
     */
    List<ActivitySkuEntity> queryActivitySkuEntitiesByActivityId(Long activityId);



    ActivityAccountDayEntity queryRaffleActivityAccountDay(Long activityId, String userId, String day);

    ActivityAccountEntity queryUserActivityAccount(Long activityId, String userId);


    Integer queryActivityAccountTotalUseCountByActivityId(String userId, Long activityId);

    void saveActivityAccountDay(ActivityAccountDayEntity build);

    void saveActivityAccountMonth(ActivityAccountMonthEntity activityAccountMonthEntity);

    void saveUserRaffleOrderEntity(UserRaffleOrderEntity userRaffleOrderEntity,Date currentDate);

    void checkMonthDayIsExist(String userId, Long activityId, Date currentDate);

    void doSaveCreditPayOrder(CreateOrderAggregate createOrderAggregate);

    void doSaveNoPayOrder(CreateOrderAggregate createOrderAggregate);

    void updateOrder(DeliveryOrderEntity deliveryOrderEntity);
}
