package org.example.domain.activity.service.quota;

import lombok.extern.slf4j.Slf4j;
import org.example.domain.activity.model.entity.*;
import org.example.domain.activity.repository.IActivityRepository;
import org.example.domain.activity.service.quota.rule.chain.Factory.DefaultActivityChainFactory;
import org.example.types.common.Constants;
import org.example.types.enums.ResponseCode;
import org.example.types.exception.AppException;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 此类为支持类
 * 用于解耦业务逻辑和具体的数据操作
 * 提供一些通用的、重复使用的功能,如封装公共查询
 * 属于模板设计模式
 */
@Slf4j
public class ActivityAccountQuotaSupport {


    protected IActivityRepository activityRepository;

    protected DefaultActivityChainFactory defaultActivityChainFactory;

    protected final Date currentDate = new Date();

    public ActivityAccountQuotaSupport(IActivityRepository activityRepository, DefaultActivityChainFactory defaultActivityChainFactory) {
        this.activityRepository = activityRepository;
        this.defaultActivityChainFactory= defaultActivityChainFactory;
    }

    public ActivityCountEntity queryActivityCountEntity(ActivitySkuEntity activitySkuEntity) {
        return activityRepository.queryActivityCountEntityById(activitySkuEntity.getActivityCountId());
    }

    public ActivityEntity queryActivityEntity(ActivitySkuEntity activitySkuEntity) {
        return activityRepository.queryActivityEntityByActivityId(activitySkuEntity.getActivityId());
    }

    public ActivitySkuEntity queryActivitySkuEntity(SkuRechargeEntity skuRechargeEntity) {
        ActivitySkuEntity activitySkuEntity = activityRepository.queryActivitySkuEntityBySku(skuRechargeEntity.getSku());
        return activitySkuEntity;
    }

}
