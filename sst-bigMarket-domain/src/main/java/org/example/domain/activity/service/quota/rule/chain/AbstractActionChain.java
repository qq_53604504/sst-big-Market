package org.example.domain.activity.service.quota.rule.chain;

/**
 * 责任链节点抽象类 ,用于 定义节点公共方法如appendNext()
 *
 */
public abstract class AbstractActionChain implements IActionChain{

    /**
     * 当前节点的下一个节点
     */
    protected IActionChain next;


    /**
     * 添加下一个节点,并返回下一个节点
     * @param next 待添加的下一个节点
     * @return 下一个节点
     */
    @Override
    public IActionChain appendNext(IActionChain next) {
        this.next = next;
        return next;
    }


}
