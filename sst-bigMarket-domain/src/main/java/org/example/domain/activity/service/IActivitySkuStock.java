package org.example.domain.activity.service;

import org.example.domain.activity.model.valobj.SubtractionSkuStockMessageVO;

/**
 * 对mysql中activity_sku表的库存进行操作的类
 */
public interface IActivitySkuStock {

    /**
     * 获取mysql减库存的消息队列
     * @param activitySkuCountQueryKey  队列的标识
     * @return  获取的消息
     */
    SubtractionSkuStockMessageVO takeQueueValue(String activitySkuCountQueryKey);


    /**
     * mysql减库存
     * @param subtractionSkuStockMessageVO 减库存消息
     * @return 是否成功
     */
    boolean subtractionSkuStock(SubtractionSkuStockMessageVO subtractionSkuStockMessageVO);

    /**
     * 设置mysql中相关sku的库存为0
     * @param sku
     */
    void clearActivitySkuStock(Long sku);

    /**
     * 清空队列中的减库存消息
     */
    void clearQueueValue(Long sku);
}
