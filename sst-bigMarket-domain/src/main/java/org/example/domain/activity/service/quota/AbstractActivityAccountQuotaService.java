package org.example.domain.activity.service.quota;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.example.domain.activity.model.aggregate.CreateOrderAggregate;
import org.example.domain.activity.model.entity.*;
import org.example.domain.activity.repository.IActivityRepository;
import org.example.domain.activity.service.IActivityAccountQuota;
import org.example.domain.activity.service.quota.policy.ITradePolicy;
import org.example.domain.activity.service.quota.rule.chain.Factory.DefaultActivityChainFactory;
import org.example.domain.activity.service.quota.rule.chain.IActionChain;
import org.example.types.enums.ResponseCode;
import org.example.types.exception.AppException;

import java.util.Map;

@Slf4j
public abstract class AbstractActivityAccountQuotaService extends ActivityAccountQuotaSupport implements IActivityAccountQuota {

    private final Map<String, ITradePolicy> tradePolicyGroup;


    public AbstractActivityAccountQuotaService(IActivityRepository raffleActivityRepository, DefaultActivityChainFactory defaultActivityChainFactory, Map<String, ITradePolicy> tradePolicyGroup) {
        super(raffleActivityRepository,defaultActivityChainFactory);
        this.tradePolicyGroup = tradePolicyGroup;
    }

    public ActivityOrderEntity createRaffleActivityOrder(ActivityShopCartEntity activityShopCartEntity){

        ActivitySkuEntity activitySkuEntity = activityRepository.queryActivitySkuEntityBySku(activityShopCartEntity.getSku());

        ActivityEntity activityEntity = activityRepository.queryActivityEntityByActivityId(activitySkuEntity.getActivityId());

        ActivityCountEntity activityCountEntity = activityRepository.queryActivityCountEntityById(activitySkuEntity.getActivityCountId());


        log.info(String.valueOf(activityEntity));
        log.info(String.valueOf(activityCountEntity));




        return ActivityOrderEntity.builder().build();
    }

    /**
     * 活动sku下单
     * 分为返利下单和积分兑换下单
     * @param skuRechargeEntity 活动商品充值实体对象
     * @return 订单号
     */
    @Override
    public String createOrder(SkuRechargeEntity skuRechargeEntity) {
        //1.参数校验
        String userId = skuRechargeEntity.getUserId();
        Long sku = skuRechargeEntity.getSku();
        if (StringUtils.isBlank(userId) || sku==null){
            throw new AppException(ResponseCode.ILLEGAL_PARAMETER.getCode(), ResponseCode.ILLEGAL_PARAMETER.getInfo());
        }

        //2.信息查询
        ActivitySkuEntity activitySkuEntity = queryActivitySkuEntity(skuRechargeEntity);
        ActivityEntity activityEntity = queryActivityEntity(activitySkuEntity);
        ActivityCountEntity activityCountEntity = queryActivityCountEntity(activitySkuEntity);


        //3.规则校验,责任链
        IActionChain actionChain = defaultActivityChainFactory.openActionChain();
        actionChain.action(activitySkuEntity,activityEntity,activityCountEntity,userId);

        //4.构建订单聚合对象
        CreateOrderAggregate createOrderAggregate = buildCreateOrderAggregate(skuRechargeEntity,activitySkuEntity,activityEntity,activityCountEntity);

        //根据下单类型来获取对应的保存订单的逻辑
        ITradePolicy tradePolicy = tradePolicyGroup.get(skuRechargeEntity.getOrderTradeType().getCode());
        tradePolicy.trade(createOrderAggregate);

        // 6. 返回单号
        return createOrderAggregate.getActivityOrderEntity().getOrderId();
    }

    protected abstract void doSaveOrder(CreateOrderAggregate createOrderAggregate);

    protected abstract CreateOrderAggregate buildCreateOrderAggregate(SkuRechargeEntity skuRechargeEntity, ActivitySkuEntity activitySkuEntity, ActivityEntity activityEntity, ActivityCountEntity activityCountEntity);


}