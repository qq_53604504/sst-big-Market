package org.example.domain.rebate.repository;

import org.example.domain.rebate.model.aggregate.BehaviorRebateAggregate;
import org.example.domain.rebate.model.entity.BehaviorRebateOrderEntity;
import org.example.domain.rebate.model.valobj.BehaviorTypeVO;
import org.example.domain.rebate.model.valobj.DailyBehaviorRebateVO;

import java.util.List;

public interface IRebateRepository {
    List<DailyBehaviorRebateVO> queryDailyBehaviorRebatesByType(BehaviorTypeVO behaviorTypeVO);

    void saveRebateRecord(List<BehaviorRebateAggregate> behaviorRebateAggregates);

    List<BehaviorRebateOrderEntity> queryRebateOrderByOutBusinessNo(String userId, String outBusinessNo);
}
