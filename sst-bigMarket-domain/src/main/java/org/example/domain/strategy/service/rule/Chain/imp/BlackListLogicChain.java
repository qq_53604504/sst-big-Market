package org.example.domain.strategy.service.rule.Chain.imp;

import lombok.extern.slf4j.Slf4j;
import org.example.domain.strategy.model.Raffle.StrategyRuleEntity;
import org.example.domain.strategy.repository.IStrategyRepository;
import org.example.domain.strategy.service.rule.Chain.AbstractLogicChain;
import org.example.domain.strategy.service.rule.Chain.factory.DefaultLogicChainFactory;
import org.example.types.common.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component("rule_blacklist")
public class BlackListLogicChain extends AbstractLogicChain {
    @Autowired
    private IStrategyRepository strategyRepository;

    @Override
    public DefaultLogicChainFactory.StrategyAwardVO logic(Long strategyId, String userId) {
        log.info("进入责任链中的黑名单逻辑");
        StrategyRuleEntity strategyRuleEntity = strategyRepository.queryStrategyRuleEntityByStrategyIdAndRuleModel(Long.valueOf(strategyId),ruleModel());

        String ruleValue = strategyRuleEntity.getRuleValue();
        String[] ruleValueKeyAndValue = ruleValue.split(Constants.COLON);
        String awardId = ruleValueKeyAndValue[0];
        String[] values = ruleValueKeyAndValue[1].split(Constants.SPLIT);
        for (String value :values){
            if (value.equals(userId)){
                return DefaultLogicChainFactory.StrategyAwardVO
                        .builder()
                        .awardId(Integer.valueOf(awardId))
                        .ruleModel(ruleModel())
                        // 写入默认配置黑名单奖品值 0.01 ~ 1 积分，也可以配置到数据库表中
                        .awardRuleValue("0.01,1")
                        .build();
            }
        }


        return next.logic(strategyId,userId);
    }

    @Override
    public String ruleModel() {
        return DefaultLogicChainFactory.LogicModel.RULE_BLACKLIST.getCode();
    }
}
