package org.example.domain.strategy.service.rule.RuleFilter.Factory;

import org.example.domain.strategy.service.rule.RuleFilter.ILogicFilter;
import org.example.domain.strategy.service.annotation.LogicStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


//这个类就是一个工厂类,主要是定义一个Map,将不同的过滤算法实现作为键值存到里面,外面的人就直接可以根据这个map用不同键值获取不同的实现,进而使用
@Component
public class DefaultFilterFactory {


    private Map<String, ILogicFilter> logicFilterMap = new ConcurrentHashMap();

    //初始化时将相关实现放进map
    //spring管理的类中的构造方法中的参数会自动被相关的同样被spring管理的bean注入,不写@Autowired也可以
    @Autowired
    DefaultFilterFactory(List<ILogicFilter> logicFilters){
        logicFilters.stream().forEach(logicFilter -> {
            LogicStrategy logicStrategy = AnnotationUtils.findAnnotation(logicFilter.getClass(), LogicStrategy.class);
            logicFilterMap.put(logicStrategy.logicModel().getCode(),logicFilter);
        });
    }

    //让外界拿到这个map
    public Map<String, ILogicFilter> openFilter(){
        return logicFilterMap;

    };


}
