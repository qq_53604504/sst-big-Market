package org.example.domain.strategy.service.rule.Chain.imp;


import lombok.extern.slf4j.Slf4j;
import org.example.domain.award.repository.IAwardRepository;
import org.example.domain.strategy.service.armory.IStrategyDispatch;
import org.example.domain.strategy.service.rule.Chain.AbstractLogicChain;
import org.example.domain.strategy.service.rule.Chain.factory.DefaultLogicChainFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 默认的责任链「作为最后一个链」
 */
@Slf4j
@Component("default")
public class DefaultLogicChain extends AbstractLogicChain {

    @Resource
    protected IStrategyDispatch strategyDispatch;

    @Autowired
    private IAwardRepository awardRepository;


    @Override
    public DefaultLogicChainFactory.StrategyAwardVO logic(Long strategyId, String userId) {
        Integer awardId = strategyDispatch.getRandomAwardId(Long.valueOf(strategyId));
        String awardConfig = awardRepository.queryAwardConfigByAwardId(awardId);
        log.info("抽奖责任链-默认处理 userId: {} strategyId: {} ruleModel: {} awardId: {}", userId, strategyId, ruleModel(), awardId);
        return DefaultLogicChainFactory.StrategyAwardVO
                .builder()
                .awardId(awardId)
                .ruleModel(ruleModel())
                .awardRuleValue(awardConfig)
                .build();
    }

    @Override
    public String ruleModel() {
        return DefaultLogicChainFactory.LogicModel.RULE_DEFAULT.getCode();
    }

}
