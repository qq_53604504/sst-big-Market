package org.example.domain.strategy.service.annotation;

import org.example.domain.strategy.model.Raffle.RuleActionEntity;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//此注解用来给IFilter的实现类加上特定的

@Target({ElementType.TYPE}) //元注解:表示可以将此注解加载在类、接口、枚举等上,不加此元注解则表示可以应用到所有元素上
@Retention(RetentionPolicy.RUNTIME) //表示该注解在程序运行时保留，可以通过反射等机制来获取注解信息,不加则没法用反射
public @interface LogicStrategy {
    RuleActionEntity.LogicModel logicModel();


}
