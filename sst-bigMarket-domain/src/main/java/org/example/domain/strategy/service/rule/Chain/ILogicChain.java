package org.example.domain.strategy.service.rule.Chain;

import org.example.domain.strategy.service.rule.Chain.factory.DefaultLogicChainFactory;

public interface ILogicChain {


    ILogicChain appendNext(ILogicChain next);

    DefaultLogicChainFactory.StrategyAwardVO logic(Long strategyId , String userId);

    String ruleModel();
}
