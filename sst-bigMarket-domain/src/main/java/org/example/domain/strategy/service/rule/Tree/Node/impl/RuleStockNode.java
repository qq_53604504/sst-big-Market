package org.example.domain.strategy.service.rule.Tree.Node.impl;


import lombok.extern.slf4j.Slf4j;
import org.example.domain.strategy.model.valobj.RuleLimitValueVO;
import org.example.domain.strategy.model.valobj.StrategyAwardStockKeyVO;
import org.example.domain.strategy.repository.IStrategyRepository;
import org.example.domain.strategy.service.armory.IStrategyDispatch;
import org.example.domain.strategy.service.rule.Tree.Engine.factory.DefaultTreeFactory;
import org.example.domain.strategy.service.rule.Tree.Node.ITreeNode;
import org.example.types.common.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Slf4j
@Component("rule_stock")
public class RuleStockNode implements ITreeNode {

    @Autowired
    private IStrategyDispatch strategyDispatch;
    @Autowired
    private IStrategyRepository strategyRepository;


    @Override
    public DefaultTreeFactory.TreeActionEntity logic(String userId, Long strategyId, String awardId,String ruleValue) {
        log.info("rule_stock执行逻辑");

        boolean status = strategyDispatch.subtractionAwardStock(Long.valueOf(strategyId),Long.valueOf(awardId));

        //为true说明库存正常
        if (status == true) {
            log.info("规则过滤-库存扣减-成功 userId:{} strategyId:{} awardId:{}", userId, strategyId, awardId);

            //这里为什么要把参数弄成一个VO类来传递,因为我们在使用队列时要传递一个泛型参数,代表这个队列接受的消息类型,因此将消息封装成一个类
            strategyRepository.awardStockConsumeSendQueue(StrategyAwardStockKeyVO.builder()
                    .awardId(awardId)
                    .strategyId(strategyId)
                    .build());

            return DefaultTreeFactory.TreeActionEntity.builder()
                    .strategyAwardData(DefaultTreeFactory.StrategyAwardVO.builder()
                            .awardId(Integer.valueOf(awardId))
                            .build())
                    .ruleLimitValueVO(RuleLimitValueVO.ALLOW)
                    .build();
        }
        log.warn("规则过滤-库存扣减-告警，库存不足。userId:{} strategyId:{} awardId:{}", userId, strategyId, awardId);
        return DefaultTreeFactory.TreeActionEntity.builder()
                .ruleLimitValueVO(RuleLimitValueVO.TAKE_OVER)
                .build();
    }

    @Override
    public DefaultTreeFactory.TreeActionEntity logic(String userId, Long strategyId, String awardId, String ruleValue, Date endDateTime) {
        log.info("rule_stock执行逻辑");

        boolean status = strategyDispatch.subtractionAwardStock(Long.valueOf(strategyId),Long.valueOf(awardId),endDateTime);

        //为true说明库存正常
        if (status == true) {
            log.info("规则过滤-库存扣减-成功 userId:{} strategyId:{} awardId:{}", userId, strategyId, awardId);

            //这里为什么要把参数弄成一个VO类来传递,因为我们在使用队列时要传递一个泛型参数,代表这个队列接受的消息类型,因此将消息封装成一个类
            strategyRepository.awardStockConsumeSendQueue(StrategyAwardStockKeyVO.builder()
                    .awardId(awardId)
                    .strategyId(strategyId)
                    .build());

            return DefaultTreeFactory.TreeActionEntity.builder()
                    .strategyAwardData(DefaultTreeFactory.StrategyAwardVO.builder()
                            .awardId(Integer.valueOf(awardId))
                            .build())
                    .ruleLimitValueVO(RuleLimitValueVO.ALLOW)
                    .build();
        }
        log.warn("规则过滤-库存扣减-告警，库存不足。userId:{} strategyId:{} awardId:{}", userId, strategyId, awardId);
        return DefaultTreeFactory.TreeActionEntity.builder()
                .ruleLimitValueVO(RuleLimitValueVO.TAKE_OVER)
                .build();
    }
}
