package org.example.domain.strategy.service.rule.RuleFilter;

import org.example.domain.strategy.model.Raffle.FilterMatterEntity;
import org.example.domain.strategy.model.Raffle.RuleActionEntity;

public interface ILogicFilter {
    RuleActionEntity<RuleActionEntity.RaffleBeforeEntity> doBeforeFilter(FilterMatterEntity filterMatterEntity);

    RuleActionEntity<RuleActionEntity.RaffleBetweenEntity> doBetweenFilter(FilterMatterEntity filterMatterEntity);

}
