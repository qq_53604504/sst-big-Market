package org.example.domain.strategy.service.rule.Tree.Node;

import org.example.domain.strategy.service.rule.Tree.Engine.factory.DefaultTreeFactory;

import java.util.Date;

public interface ITreeNode {

    /**
     * 不带活动截至时间的逻辑
     * @param userId
     * @param strategyId
     * @param awardId
     * @param ruleValue
     * @return
     */
    //真实的执行函数接口,engine实现类中用这个返回值的RuleLimitValueVO来判断逻辑走向
    DefaultTreeFactory.TreeActionEntity logic(String userId , Long strategyId , String awardId,String ruleValue);

    /**
     * 带活动结束时间的逻辑
     * @param userId
     * @param strategyId
     * @param awardId
     * @param ruleValue
     * @param endDateTime 活动结束时间,用于库存节点中的redis的奖品库存key设置过期时间
     * @return
     */
    DefaultTreeFactory.TreeActionEntity logic(String userId, Long strategyId, String awardId, String ruleValue, Date endDateTime);
}
