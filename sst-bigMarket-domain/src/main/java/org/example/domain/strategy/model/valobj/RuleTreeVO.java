package org.example.domain.strategy.model.valobj;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;



@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RuleTreeVO {

    private String ruleTreeId;

    private String rootNodeKey;

    private String ruleTreeDesc;

    private Map<String , RuleTreeNodeVO> ruleTreeNodeVOMap;

}
