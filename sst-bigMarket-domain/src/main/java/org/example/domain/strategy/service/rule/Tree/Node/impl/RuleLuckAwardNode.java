package org.example.domain.strategy.service.rule.Tree.Node.impl;


import lombok.extern.slf4j.Slf4j;
import org.example.domain.strategy.model.valobj.RuleLimitValueVO;
import org.example.domain.strategy.service.rule.Tree.Engine.factory.DefaultTreeFactory;
import org.example.domain.strategy.service.rule.Tree.Node.ITreeNode;
import org.example.types.common.Constants;
import org.springframework.stereotype.Component;

import java.util.Date;

@Slf4j
@Component("rule_luck_award")
public class RuleLuckAwardNode implements ITreeNode {
    @Override
    public DefaultTreeFactory.TreeActionEntity logic(String userId, Long strategyId, String awardId,String ruleValue) {
        log.info("rule_luck_award执行逻辑");
        String[] split = ruleValue.split(Constants.COLON);
        if (split == null) {
            log.error("规则过滤-兜底奖品，兜底奖品未配置告警 userId:{} strategyId:{} awardId:{}", userId, strategyId, awardId);
            throw new RuntimeException("兜底奖品未配置 " + ruleValue);
        }
        String ruleAwardId = split[0];

        //这样写是为了处理像101而不是101:1,100的情况
        String awardRuleValue = split.length>1?split[1]:"";

        return DefaultTreeFactory.TreeActionEntity.builder()
                .ruleLimitValueVO(RuleLimitValueVO.TAKE_OVER)
                .strategyAwardData(DefaultTreeFactory.StrategyAwardVO.builder()
                        .awardId(Integer.valueOf(ruleAwardId))
                        .awardRuleValue(awardRuleValue)
                        .build())
                .build();
    }

    @Override
    public DefaultTreeFactory.TreeActionEntity logic(String userId, Long strategyId, String awardId, String ruleValue, Date endDateTime) {
        log.info("rule_luck_award执行逻辑");
        String[] split = ruleValue.split(Constants.COLON);
        if (split == null) {
            log.error("规则过滤-兜底奖品，兜底奖品未配置告警 userId:{} strategyId:{} awardId:{}", userId, strategyId, awardId);
            throw new RuntimeException("兜底奖品未配置 " + ruleValue);
        }
        String ruleAwardId = split[0];

        //这样写是为了处理像101而不是101:1,100的情况
        String awardRuleValue = split.length>1?split[1]:"";

        return DefaultTreeFactory.TreeActionEntity.builder()
                .ruleLimitValueVO(RuleLimitValueVO.TAKE_OVER)
                .strategyAwardData(DefaultTreeFactory.StrategyAwardVO.builder()
                        .awardId(Integer.valueOf(ruleAwardId))
                        .awardRuleValue(awardRuleValue)
                        .build())
                .build();
    }
}
