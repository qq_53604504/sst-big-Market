package org.example.domain.strategy.service.armory;

import org.example.domain.strategy.model.Raffle.StrategyAwardEntity;

import java.util.Date;
import java.util.List;

public interface IStrategyDispatch {

    Integer getRandomAwardId(Long strategyId);

    Integer getRandomAwardId(Long strategyId, String ruleWeightValue);

    Boolean subtractionAwardStock(Long strategyId, Long awardId);

    List<StrategyAwardEntity> getAwardEntityList(Long strategyId);
    List<StrategyAwardEntity> getAwardEntityListByActivityId(Long activityId);

    boolean subtractionAwardStock(Long strategyId, Long awardId, Date endDateTime);
}
