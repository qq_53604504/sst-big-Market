package org.example.domain.strategy.service.rule.Chain.factory;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.example.domain.strategy.model.Raffle.StrategyEntity;
import org.example.domain.strategy.repository.IStrategyRepository;
import org.example.domain.strategy.service.rule.Chain.ILogicChain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Slf4j
@Component
public class DefaultLogicChainFactory {

    Map<String , ILogicChain> logicChainMap;

    @Autowired
    private IStrategyRepository strategyRepository;

    public DefaultLogicChainFactory(Map<String , ILogicChain> logicChainMap) {
        this.logicChainMap = logicChainMap;
    }
    public  ILogicChain openLogicChain(Long strategyId){
        StrategyEntity strategyEntity = strategyRepository.queryStrategyEntityByStrategyId(Long.valueOf(strategyId));
        String[] ruleModels = strategyEntity.getRuleModels();
        ILogicChain logicChain = null;
        if (ruleModels != null) {//为空说明没有配置前置规则
            logicChain = logicChainMap.get(ruleModels[0]);
            ILogicChain current = logicChain;
            for (int i = 1; i < ruleModels.length; i++) {
                current = current.appendNext(logicChainMap.get(ruleModels[i]));
            }
            current.appendNext(logicChainMap.get("default"));
        }else logicChain=logicChainMap.get("default");


        log.info("责任链获取成功");
        return logicChain;

    }


    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class StrategyAwardVO {
        /** 抽奖奖品ID - 内部流转使用 */
        private Integer awardId;
        /** 抽奖奖品规则 */
        private String  ruleModel;
        /**
         * 抽奖奖品规则,黑名单的用于固定范围内的积分
         */
        private String awardRuleValue;


    }


    @Getter
    @AllArgsConstructor
    public enum LogicModel {

        RULE_DEFAULT("rule_default", "默认抽奖"),
        RULE_BLACKLIST("rule_blacklist", "黑名单抽奖"),
        RULE_WEIGHT("rule_weight", "权重规则"),
        ;

        private final String code;
        private final String info;

    }


}
