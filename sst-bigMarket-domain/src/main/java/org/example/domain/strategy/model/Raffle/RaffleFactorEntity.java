package org.example.domain.strategy.model.Raffle;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class RaffleFactorEntity {

    private Long strategyId;
    private String userId;

    /** 结束时间 */
    private Date endDateTime;

}
