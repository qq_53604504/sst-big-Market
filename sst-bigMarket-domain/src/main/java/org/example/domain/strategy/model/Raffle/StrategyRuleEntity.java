package org.example.domain.strategy.model.Raffle;

import lombok.Builder;
import lombok.Data;
import org.example.types.common.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 策略规则实体
 */

@Data
@Builder
public class StrategyRuleEntity {
    private Integer strategyId;
    private Integer awardId;
    private Integer ruleType;
    /**
     * 策略规则 (被包含于策略实体中的ruleModels字段)
     */
    private String ruleModel;
    /**
     * 规则值(用于规则处理)
     */
    private String ruleValue;
    private String ruleDesc;

    /**
     * 获取权重值:并转换为键为门槛值,值为奖品范围列表的Map
     * 数据案例；4000:102,103,104,105 5000:102,103,104,105,106,107 6000:102,103,104,105,106,107,108,109
     */
    public Map<String, List<Integer>> getRuleValuesMap() {


        Map<String, List<Integer>> ruleValueMap = new HashMap<>();
        String[] ruleValues = ruleValue.split(Constants.SPACE);
        for (String ruleValue : ruleValues){
            //数据库中的字段可能以两个空格隔开,这样分割时会得到一个""的字符串,因此要判断
            if ( ruleValue == null || ruleValue.isEmpty()) return ruleValueMap;
            String[] keyAndValue = ruleValue.split(Constants.COLON);
            //IllegalArgumentException表示参数不合法判断,可以用这个来验证数据库的字段是否符合要求
            if (keyAndValue.length != 2) {
                throw new IllegalArgumentException("rule_weight rule_rule invalid input format" + keyAndValue);
            }
            String key = keyAndValue[0];
            String valueGroup = keyAndValue[1];
            List<Integer> valueList = new ArrayList<>();
            String[] values = valueGroup.split(Constants.SPLIT);
            for (String value : values){
                valueList.add(Integer.valueOf(value));
            }

            ruleValueMap.put(ruleValue,valueList);


        }
        return ruleValueMap;


    }
}
