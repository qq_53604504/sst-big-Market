package org.example.domain.strategy.model.Raffle;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.example.types.common.Constants;

/**
 * 策略实体
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StrategyEntity {

    private Integer strategyId;

    private String strategyDesc;

    /**
     * 策略规则列表
     */
    private String ruleModels;

    public String[] getRuleModels(){
//        if (!ruleModels.isEmpty()&&!ruleModels.equals("") )
//        return ruleModels.split(Constants.SPLIT);
//        else return null;
        if (StringUtils.isBlank(ruleModels)) return null;
        return ruleModels.split(Constants.SPLIT);
    }




    public String getRuleWeight() {
        String[] ruleModels = getRuleModels();

        //这个判空很重要,不管是否自己主动调用,Fastjson 序列化对象时，都会调用对象的所有 get 方法来获取属性值，并尝试将其转换为 JSON 字符串,
        //不判空则在执行后面语句时会报空指针异常
        if (null == ruleModels) return null;

        for (String ruleModel : ruleModels){
            if (ruleModel.equals("rule_weight"))
                return ruleModel;
        }
        return null;
    }
}
