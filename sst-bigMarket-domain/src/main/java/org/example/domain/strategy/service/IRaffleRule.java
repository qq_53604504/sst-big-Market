package org.example.domain.strategy.service;

import org.example.domain.strategy.model.valobj.RuleWeightVO;

import java.util.List;
import java.util.Map;

/**
 *
 */
public interface IRaffleRule {

    Map<String,Integer> queryRuleLockValue(String[] treeIds);


    List<RuleWeightVO> queryAwardRuleWeightByActivityId(Long activityId);
}
