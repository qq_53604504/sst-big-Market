package org.example.domain.strategy.service.rule.Chain;

import org.example.domain.strategy.service.rule.Chain.factory.DefaultLogicChainFactory;

public abstract class AbstractLogicChain implements ILogicChain{
    public ILogicChain next;

    public ILogicChain appendNext(ILogicChain next) {
        this.next = next;
        return next;
    }

    public abstract DefaultLogicChainFactory.StrategyAwardVO logic(Long strategyId , String userId);

     public abstract String ruleModel();



}
