package org.example.domain.strategy.model.valobj;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RuleLogicCheckTypeVO {

    ALLOW("400","放行,未被接管"),
    TAKE_OVER("401","接管,执行过滤操作");

    private String code;
    private String desc;


}
