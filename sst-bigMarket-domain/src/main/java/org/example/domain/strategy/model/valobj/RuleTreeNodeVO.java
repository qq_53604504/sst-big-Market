package org.example.domain.strategy.model.valobj;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RuleTreeNodeVO {

    private String nodeKey;

    private String ruleValue;

    private List<RuleTreeNodeLineVO> ruleTreeNodeLineVOList;

}
