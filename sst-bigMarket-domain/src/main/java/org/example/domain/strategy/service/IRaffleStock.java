package org.example.domain.strategy.service;

import org.example.domain.strategy.model.valobj.StrategyAwardStockKeyVO;


/**
 *
 */
//该文件用来给更新mysql库存时提供方法
public interface IRaffleStock {
    StrategyAwardStockKeyVO takeQueueValue();

    void updateStrategyAwardStock(Long strategyId, String awardId);
}
