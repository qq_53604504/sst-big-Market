package org.example.domain.strategy.service.armory;

/**
 * 策略装配接口
 */
public interface IStrategyArmory {

    /**
     * 装配策略 包括奖品抽奖表 ,奖品库存
     * @param StrategyId 策略id
     * @return
     */
    boolean assembleLotteryStrategy(Long StrategyId);


    /**
     * 根据活动id装配策略 包括奖品抽奖表 ,奖品库存
     * @param activityId 活动id
     * @return
     */
    boolean assembleLotteryStrategyByActivityId(Long activityId);
}