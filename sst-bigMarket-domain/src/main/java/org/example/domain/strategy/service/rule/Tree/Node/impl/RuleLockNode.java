package org.example.domain.strategy.service.rule.Tree.Node.impl;

import lombok.extern.slf4j.Slf4j;
import org.example.domain.strategy.model.valobj.RuleLimitValueVO;
import org.example.domain.strategy.repository.IStrategyRepository;
import org.example.domain.strategy.service.rule.Tree.Engine.factory.DefaultTreeFactory;
import org.example.domain.strategy.service.rule.Tree.Node.ITreeNode;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 次数锁节点
 * 查询用户抽奖次数,判断是否解除次数锁限制
 */
@Slf4j
@Component("rule_lock")
public class RuleLockNode implements ITreeNode {

    @Resource
    private IStrategyRepository strategyRepository;

    @Override
    public DefaultTreeFactory.TreeActionEntity logic(String userId, Long strategyId, String awardId,String ruleValue) {
        log.info("rule_lock执行逻辑");

        Long ruleCount = Long.valueOf(ruleValue);

        Integer userRaffleCount = strategyRepository.queryTodayUserRaffleCount(userId, strategyId);

        //用户数值小于规则数值,走下放行逻辑
        if (ruleCount > userRaffleCount){
            return DefaultTreeFactory.TreeActionEntity.builder()
                    .ruleLimitValueVO(RuleLimitValueVO.ALLOW)
                    .build();
        }

        //用户数值大于规则数值,走接管接管
        return DefaultTreeFactory.TreeActionEntity.builder()
                 .ruleLimitValueVO(RuleLimitValueVO.TAKE_OVER)
                .build();
    }

    @Override
    public DefaultTreeFactory.TreeActionEntity logic(String userId, Long strategyId, String awardId, String ruleValue, Date endDateTime) {
        log.info("rule_lock执行逻辑");

        Long ruleCount = Long.valueOf(ruleValue);

        Integer userRaffleCount = strategyRepository.queryTodayUserRaffleCount(userId, strategyId);

        //用户数值小于规则数值,走下放行逻辑
        if (ruleCount > userRaffleCount){
            return DefaultTreeFactory.TreeActionEntity.builder()
                    .ruleLimitValueVO(RuleLimitValueVO.ALLOW)
                    .build();
        }

        //用户数值大于规则数值,走接管接管
        return DefaultTreeFactory.TreeActionEntity.builder()
                .ruleLimitValueVO(RuleLimitValueVO.TAKE_OVER)
                .build();
    }


}
