package org.example.domain.strategy.model.valobj;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RuleTreeNodeLineVO {

    String fromNodeKey;

    String toNodeKey;

    RuleLimitTypeVO limitType;

    RuleLimitValueVO limitValue;




}
