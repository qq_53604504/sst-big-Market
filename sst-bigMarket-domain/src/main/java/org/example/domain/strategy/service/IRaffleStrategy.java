package org.example.domain.strategy.service;

import org.example.domain.strategy.model.Raffle.RaffleAwardEntity;
import org.example.domain.strategy.model.Raffle.RaffleFactorEntity;

/**
 * 抽奖策略接口
 */
public interface IRaffleStrategy {

    /**
     * 执行抽奖
     * @param raffleFactorEntity  抽奖因子:用户id,活动id等
     * @return 抽奖结果奖品实体:奖品信息
     */
    RaffleAwardEntity performRaffle(RaffleFactorEntity raffleFactorEntity);


}
