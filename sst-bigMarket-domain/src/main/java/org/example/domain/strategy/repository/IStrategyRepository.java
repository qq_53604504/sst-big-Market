package org.example.domain.strategy.repository;

import org.example.domain.strategy.model.Raffle.StrategyAwardEntity;
import org.example.domain.strategy.model.Raffle.StrategyEntity;
import org.example.domain.strategy.model.Raffle.StrategyRuleEntity;
import org.example.domain.strategy.model.valobj.RuleTreeVO;
import org.example.domain.strategy.model.valobj.RuleWeightVO;
import org.example.domain.strategy.model.valobj.StrategyAwardRuleModelVO;
import org.example.domain.strategy.model.valobj.StrategyAwardStockKeyVO;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface IStrategyRepository {
    List<StrategyAwardEntity> queryStrategyAwardList(Long strategyId);

    void storeStrategyAwardSearchRateTable(String key , Integer rateRange,Map strategyAwardSearchRateTables);

    Integer getRateRangeByStrategyId(Long strategyId);

    Integer getAwardIdByRandomIndex(Long strategyId ,Integer randomIndex);

    StrategyEntity queryStrategyEntityByStrategyId(Long strategyId);

    StrategyRuleEntity queryStrategyRuleEntityByStrategyIdAndRuleModel(Long strategyId, String ruleModel);

    int getRateRange(String key);

    Integer getAwardId(String key, Integer randomIndex);

    StrategyAwardRuleModelVO queryStrategyAwardRuleModelVO(Long strategyId, Integer awardId);

    StrategyRuleEntity queryStrategyRuleEntityByStrategyIdAndRuleModelAndAwardId(String strategyId, String ruleModel, String awardId);

    RuleTreeVO queryRuleTreeVOByTreeId(String treeId);


    void cacheStrategyAwardCount(String cacheKey, Long awardCount);

    boolean subtractionAwardStockInRedis(String key);

    void awardStockConsumeSendQueue(StrategyAwardStockKeyVO strategyAwardStockKeyVO);

    StrategyAwardStockKeyVO takeQueueValue();

    void updateStrategyAwardStock(Long strategyId, String awardId);

    StrategyAwardEntity queryStrategyAwardEntity(Long strategyId, Integer awardId);

    Long queryStrategyIdByActivityId(Long activityId);

    Integer queryTodayUserRaffleCount(String userId, Long strategyId);

    Integer queryRuleValueByTreeIdAndRuleKey(String treeId,String ruleKey);

    boolean subtractionAwardStockInRedis(String key, Date endDateTime);

    List<RuleWeightVO> queryAwardRuleWeightByStrategyId(Long strategyId);

    Integer queryActivityAccountTotalUseCount(String userId, Long strategyId);
}
