package org.example.domain.strategy.service.rule.Tree.Engine;

import org.example.domain.strategy.service.rule.Tree.Engine.factory.DefaultTreeFactory;

import java.util.Date;

public interface IDecisionTreeEngine {

    DefaultTreeFactory.StrategyAwardVO pocess(String userId, Long strategyId, String awardId);

    DefaultTreeFactory.StrategyAwardVO pocess(String userId, Long strategyId, String s, Date endDateTime);
}
