package org.example.domain.strategy.service.rule.Tree.Engine.factory;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.domain.strategy.model.valobj.RuleLimitValueVO;
import org.example.domain.strategy.model.valobj.RuleTreeVO;
import org.example.domain.strategy.service.rule.Tree.Engine.DecisionTreeEngine;
import org.example.domain.strategy.service.rule.Tree.Engine.IDecisionTreeEngine;
import org.example.domain.strategy.service.rule.Tree.Node.ITreeNode;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;


//做一个整合操作
//treeNode需要构造函数传递
//treeNodeVO需要参数传递
//这个工厂类将两个整合一起传给实现类并返回
@Service
public class DefaultTreeFactory {

    Map<String, ITreeNode> treeNodeMap = new HashMap<>();


    public DefaultTreeFactory(Map<String, ITreeNode> treeNodeMap) {
        this.treeNodeMap=treeNodeMap;

    }

    public IDecisionTreeEngine openLogicTree(RuleTreeVO ruleTreeVO){ //返回实现类
        return new DecisionTreeEngine(treeNodeMap ,ruleTreeVO); //将工厂类注入的treeNode连同参数传递的ruleTreeVO一起传给具体的实现类

    }


    /**
     *
     */
    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class TreeActionEntity {
        private RuleLimitValueVO ruleLimitValueVO;
        private StrategyAwardVO strategyAwardData;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class StrategyAwardVO {
        /** 抽奖奖品ID - 内部流转使用 */
        private Integer awardId;
        /** 抽奖奖品规则 */
        private String awardRuleValue;
    }

}
