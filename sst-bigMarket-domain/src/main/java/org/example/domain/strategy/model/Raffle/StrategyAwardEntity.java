package org.example.domain.strategy.model.Raffle;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;


//奖品配置结果
@Data
@Builder
public class StrategyAwardEntity {

    private Integer strategyId;
    private Integer awardId;
    private String ruleModels;
    private Integer awardCount;
    private Integer awardCountSurplus;
    private BigDecimal awardRate;
    // 奖品标题
    private String awardTitle;
    // 奖品副标题【抽奖1次后解锁】
    private String awardSubtitle;
    // 排序编号
    private Integer sort;
}
