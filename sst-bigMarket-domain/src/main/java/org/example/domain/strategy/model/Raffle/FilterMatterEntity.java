package org.example.domain.strategy.model.Raffle;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FilterMatterEntity {

    private String strategyId;
    private String userId;
    private String ruleModel;
    private String awardId;

}
