package org.example.domain.strategy.model.Raffle;

import lombok.Builder;
import lombok.Data;

/**
 * 抽奖结果奖品返回实体
 */

@Data
@Builder
public class RaffleAwardEntity {

    private Long strategyId;
    private Integer awardId;

    /** 奖品配置信息 */
    private String awardConfig;
    private String awardTitle;
    private Integer sort;
}
