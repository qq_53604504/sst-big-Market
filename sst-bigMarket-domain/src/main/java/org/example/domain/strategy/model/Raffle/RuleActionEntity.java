package org.example.domain.strategy.model.Raffle;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

//不同时刻在过滤时返回的结果是不同的,如在抽奖前的黑名单过滤返回的是awardId,权重策略返回的是权重值,抽奖中过滤的则不是这些值,抽奖后过滤也不是
//那么有多少个策略规则就要创建多少个返回类吗? 可以不用,直接在类中定义类就可以了,在类中定义三个子类分别代表代表抽奖前过滤,抽奖中过滤,抽奖后过滤,
//那如何让外面使用的人知道应该要选择哪个子类来进行传输呢? 给这个总类定义一个模板,再定义一个变量T data ,外面传什么类型,这个变量data就是什么类型,就可以做到根据不同时刻传不同类型
//如何限制外面传入的类在一定的范围之中呢,如只传自己定义的类? 使用一个空的静态内部类作为父类,让自定义的这几个范围类继承这个空父类,然后用RuleActionEntity <T extends RuleActionEntity.RaffleEntity>就可以限定传入的T在自己想要的范围内
@Data
@Builder
public class RuleActionEntity <T extends RuleActionEntity.RaffleEntity>{

    String codeAllowOrTakeOver ;

    String ruleModel;

    T data;

    //内部类一般是静态类
    static class RaffleEntity{

    }
    @Data
    @Builder
   public static class RaffleBeforeEntity extends  RaffleEntity{
        private String awardId;
        private String ruleWeightValueKey;
        private String strategyId;

    }
    @Data
    @Builder
    public static class  RaffleBetweenEntity extends  RaffleEntity{
        private String awardId;
        private String ruleWeightValueKey;
        private String strategyId;
    }
    @Data
    @Builder
    static class RaffleAfterEntity extends  RaffleEntity{

    }

    @Getter
    @AllArgsConstructor
    public enum LogicModel {

        //加一个type标签用于区分是过滤中规则还是过滤后还是过滤前,因为过滤中和过滤后规则用的是同一张表strategy_award的同一个字段,都是关于奖品的规则,所以查数据库的时候是两种规则一起查出来,因此我们要在编程时区分开
        RULE_WIGHT("rule_weight","【抽奖前规则】根据抽奖权重返回可抽奖范围KEY","before"),
        RULE_BLACKLIST("rule_blacklist","【抽奖前规则】黑名单规则过滤，命中黑名单则直接返回","before"),
        RULE_LOCK("rule_lock", "【抽奖中规则】抽奖n次后，对应奖品可解锁抽奖", "between"),
        RULE_LUCK_AWARD("rule_luck_award", "【抽奖后规则】抽奖n次后，对应奖品可解锁抽奖", "after"),

        ;

        private final String code;
        private final String info;
        private final String type;

        //判断类型,过滤中还是过滤后
        public static boolean isBetween(String ruleModelValue) {
            if (LogicModel.valueOf(ruleModelValue.toUpperCase()).type.equals("between")){
                return true;
            }
            return false;
        }

        public static boolean isAfter(String ruleModelValue) {
            if (LogicModel.valueOf(ruleModelValue.toUpperCase()).type.equals("after")){
                return true;
            }
            return false;
        }
    }
}
