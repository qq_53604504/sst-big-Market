package org.example.trigger.listener;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.example.domain.activity.model.entity.SkuRechargeEntity;
import org.example.domain.activity.service.IActivityAccountQuota;
import org.example.domain.credit.model.entity.TradeEntity;
import org.example.domain.credit.model.valobj.TradeNameVO;
import org.example.domain.credit.model.valobj.TradeTypeVO;
import org.example.domain.credit.service.ICreditAdjustService;
import org.example.domain.rebate.event.SendRebateMessageEvent;
import org.example.domain.rebate.model.valobj.RebateTypeVO;
import org.example.types.enums.ResponseCode;
import org.example.types.event.BaseEvent;
import org.example.types.exception.AppException;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 用户行为返利入库消息消费者
 * sku下单 || 积分调额下单
 */
@Slf4j
@Component
@RocketMQMessageListener(consumerGroup = "sst-group-send-rebate", topic = "send_rebate")
public class RebateMessageCustomer implements RocketMQListener<String> {

    private String topic= "send_rebate";
    @Resource
    private IActivityAccountQuota activityAccountQuota;

    @Resource
    private ICreditAdjustService creditAdjustService;

    @Override
    public void onMessage(String message) {
        try {
            log.info("mq接受消息");
            System.out.println(message);
            BaseEvent.EventMessage<SendRebateMessageEvent.RebateMessage> eventMessage = JSON.parseObject(message, new TypeReference<BaseEvent.EventMessage<SendRebateMessageEvent.RebateMessage>>() {
            }.getType());

            SendRebateMessageEvent.RebateMessage data = eventMessage.getData();
            String messageId = eventMessage.getId();
            Date timestamp = eventMessage.getTimestamp();
            String userId = data.getUserId();
            String bizId = data.getBizId();
            String rebateConfig = data.getRebateConfig();
            String rebateType = data.getRebateType();
            String rebateDesc = data.getRebateDesc();
            switch (RebateTypeVO.SKU.getCode()){
                case "sku":
                    activityAccountQuota.createOrder(SkuRechargeEntity.builder()
                            .sku(Long.valueOf(rebateConfig))
                            .outBusinessNo(bizId)
                            .userId(userId)
                            .build());
                    break;
                case "integral":
                    creditAdjustService.createOrder(TradeEntity.builder()
                            .amount(new BigDecimal(rebateConfig))
                            .outBusinessNo(bizId)
                            .tradeName(TradeNameVO.REBATE)
                            .tradeType(TradeTypeVO.FORWARD)
                            .userId(userId)
                            .build());
            }
        } catch (AppException e) {
            if (ResponseCode.INDEX_DUP.getCode().equals(e.getCode())) {
                log.warn("监听用户行为返利消息，消费重复 topic: {} message: {}", topic, message, e);
                return;
            }
        }catch (Exception e) {
            log.error("监听用户行为返利消息，消费失败 topic: {} message: {}", topic, message, e);
            throw e;
        }

    }
}
