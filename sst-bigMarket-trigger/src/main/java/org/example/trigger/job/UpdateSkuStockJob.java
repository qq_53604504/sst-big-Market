package org.example.trigger.job;

import lombok.extern.slf4j.Slf4j;
import org.example.domain.activity.model.valobj.SubtractionSkuStockMessageVO;
import org.example.domain.activity.service.IActivitySkuStock;
import org.example.types.common.Constants;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@Slf4j
public class UpdateSkuStockJob {


    @Resource
    private IActivitySkuStock skuStock;

    @Scheduled(cron = "0/5 * * * * ?")
    public void exec() {
        log.info("定时任务，更新活动sku库存【延迟队列获取，降低对数据库的更新频次，不要产生竞争】");
        SubtractionSkuStockMessageVO subtractionSkuStockMessageVO = skuStock.takeQueueValue(Constants.RedisKey.ACTIVITY_SKU_COUNT_QUERY_KEY);
        if (subtractionSkuStockMessageVO == null){
            log.info("队列信息获取失败");
            return;
        }

        log.info("队列信息获取成功,开始扣减库存");
        Boolean statue = skuStock.subtractionSkuStock(subtractionSkuStockMessageVO);
        if (statue)
            log.info("库存扣减成功");

    }

}
