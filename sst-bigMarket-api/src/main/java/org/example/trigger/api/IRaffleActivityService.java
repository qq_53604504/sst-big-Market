package org.example.trigger.api;


import org.example.trigger.api.dto.ActivityDrawRequestDTO;
import org.example.trigger.api.dto.ActivityDrawResponseDTO;
import org.example.trigger.api.dto.UserActivityAccountRequestDTO;
import org.example.trigger.api.dto.UserActivityAccountResponseDTO;
import org.example.types.model.Response;

import java.text.SimpleDateFormat;

/**
 * 抽奖活动服务
 */
public interface IRaffleActivityService {

    /**
     * 活动装配，数据预热缓存
     * @param activityId 活动ID
     * @return 装配结果
     */
    Response<Boolean> armory(Long activityId);

    /**
     * 活动抽奖接口
     * @param request 请求对象
     * @return 返回结果
     */
    Response<ActivityDrawResponseDTO> draw(ActivityDrawRequestDTO request);

    Response<Boolean> calendarRebate(String userId, String behaviorTypeCode, String outBusinessNo);


    Response<Boolean> isCalendarSignRebate(String userId, String outBusinessNo);

    /**
     * 查询相关策略权重信息
     * 即目前能够抽到的哪些奖品
     * @param requestDTO 用户id ,活动id
     * @return
     */
    Response<UserActivityAccountResponseDTO>  queryUserActivityAccount(UserActivityAccountRequestDTO requestDTO);
}
