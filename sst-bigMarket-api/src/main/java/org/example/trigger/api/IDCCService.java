package org.example.trigger.api;


import org.example.types.model.Response;

/**
 * DCC 动态配置中心
 */
public interface IDCCService {

    Response<Boolean> updateConfig(String key, String value);

}
