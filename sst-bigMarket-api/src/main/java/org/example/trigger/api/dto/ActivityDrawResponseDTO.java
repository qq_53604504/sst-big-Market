package org.example.trigger.api.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ActivityDrawResponseDTO implements Serializable {

    private Integer awardId;

    private String awardDesc;

    // 排序编号【策略奖品配置的奖品顺序编号】
    private Integer awardIndex;

}
