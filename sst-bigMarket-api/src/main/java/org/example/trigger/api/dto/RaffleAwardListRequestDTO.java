package org.example.trigger.api.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 抽奖奖品列表，请求对象
 */
@Data
public class RaffleAwardListRequestDTO implements Serializable {


    //活动id
    private Long activityId;
    //用户id
    private String userId;

}
