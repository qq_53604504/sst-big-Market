package org.example.infrastructure.persistent.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 策略相关的奖品信息
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StrategyAward {

    private Integer id;
    private Integer strategyId;
    private Integer awardId;
    private String awardTitle;
    private String awardSubtitle;
    private Integer awardCount;
    private Integer awardCountSurplus;
    private BigDecimal awardRate;
    /**
     * 奖品规则(抽奖后规则)
     */
    private String ruleModels;
    private Integer sort;
    private Date createTime;
    private Date updateTime;
}
