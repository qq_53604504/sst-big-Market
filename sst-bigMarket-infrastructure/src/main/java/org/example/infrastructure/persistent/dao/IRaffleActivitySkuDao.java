package org.example.infrastructure.persistent.dao;

import org.apache.ibatis.annotations.Mapper;
import org.example.infrastructure.persistent.po.RaffleActivitySku;

import java.util.List;

@Mapper
public interface IRaffleActivitySkuDao {


    RaffleActivitySku queryRaffleActivitySkuBySku(Long sku);


    Integer subtractionSkuStock(Long sku);

    void clearActivitySkuStock(Long sku);

    List<RaffleActivitySku> queryActivitySkuEntitiesByActivityId(Long activityId);
}
