package org.example.infrastructure.persistent.dao;


import cn.bugstack.middleware.db.router.annotation.DBRouter;
import org.apache.ibatis.annotations.Mapper;
import org.example.infrastructure.persistent.po.RaffleActivityAccountDay;

/**

 * 抽奖活动账户表-日次数

 */
@Mapper
public interface IRaffleActivityAccountDayDao {

    @DBRouter(key = "userId")
    RaffleActivityAccountDay queryActivityAccountDayByUserId(RaffleActivityAccountDay raffleActivityAccountDay);

    int updateActivityAccountDaySubtractionQuota(RaffleActivityAccountDay raffleActivityAccountDay);

    void insertActivityAccountDay(RaffleActivityAccountDay raffleActivityAccountDay);

    int updateActivityAccountDayAddQuota(RaffleActivityAccountDay raffleActivityAccountDay);
}
