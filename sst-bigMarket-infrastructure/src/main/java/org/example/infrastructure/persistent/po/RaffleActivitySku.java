package org.example.infrastructure.persistent.po;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class RaffleActivitySku {

    private Long id;

    /**
     * 商品sku
     */
    private Long sku;


   private Long activityId;

   private Long activityCountId;

   private Long stockCount;

   private Long stockCountSurplus;

    /**
     * sku的积分价格
     */
   private BigDecimal productAmount;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

}
