package org.example.infrastructure.persistent.dao;

import cn.bugstack.middleware.db.router.annotation.DBRouter;
import org.apache.ibatis.annotations.Mapper;
import org.example.infrastructure.persistent.po.RaffleActivityAccount;
import org.example.infrastructure.persistent.po.RaffleActivityAccountDay;

/**
 * 抽奖活动账户表
 */
@Mapper
public interface IRaffleActivityAccountDao {

    @DBRouter
    RaffleActivityAccount queryRaffleActivityAccountByUserId(RaffleActivityAccount RaffleActivityAccountReq);


    int updateAccountQuota(RaffleActivityAccount raffleActivityAccount);

    void createAccount(RaffleActivityAccount raffleActivityAccount);

    int updateActivityAccountSubtractionQuota(RaffleActivityAccount raffleActivityAccount);


}
