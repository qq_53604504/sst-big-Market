package org.example.infrastructure.persistent.dao;

import org.apache.ibatis.annotations.Mapper;
import org.example.infrastructure.persistent.po.UserCreditAccount;

@Mapper
public interface IUserCreditAccountDao {

    void insert(UserCreditAccount userCreditAccount);

    int updateAddAmount(UserCreditAccount userCreditAccount);

}
