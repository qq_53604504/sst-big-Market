package org.example.infrastructure.persistent.po;

import lombok.Data;

import java.util.Date;

/**
 * 策略规则
 */

@Data
public class StrategyRule {
private Integer id;
private Integer strategyId;
private Integer awardId;
private Integer ruleType;
private String ruleModel;
private String ruleValue;
private String ruleDesc;
private Date createTime;
private Date updateTime;
}
