package org.example.infrastructure.persistent.repository;

import org.checkerframework.checker.units.qual.A;
import org.example.domain.award.event.SendAwardMessageEvent;
import org.example.domain.task.model.entity.TaskEntity;
import org.example.domain.task.repository.ITaskRepository;
import org.example.infrastructure.event.EventPublisher;
import org.example.infrastructure.persistent.dao.ITaskDao;
import org.example.infrastructure.persistent.po.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;


@Repository
public class TaskRepository implements ITaskRepository {

    @Autowired
    private ITaskDao taskDao;

    @Autowired
    private EventPublisher eventPublisher;

    @Autowired
    private SendAwardMessageEvent sendAwardMessageEvent;

    @Override
    public List<TaskEntity> queryNoSendMessageTaskList() {
        List<TaskEntity> taskEntities = new ArrayList<>();

        List<Task> tasks = taskDao.queryNoSendMessageTaskList();

        for (Task task :tasks){
            TaskEntity taskEntity = TaskEntity.builder()
                        .userId(task.getUserId())
                        .topic(task.getTopic())
                        .messageId(task.getMessageId())
                        .message(task.getMessage())
                        .build();

            taskEntities.add(taskEntity);
        }

        return taskEntities;

    }

    @Override
    public void sendMessage(TaskEntity taskEntity) {




        eventPublisher.publish(taskEntity.getTopic(),taskEntity.getMessage());

    }

    @Override
    public void updateTaskSendMessageCompleted(String userId, String messageId) {

        Task taskReq = new Task() ;
        taskReq.setMessageId(messageId);
        taskReq.setUserId(userId);

        taskDao.updateTaskSendMessageCompleted(taskReq);
    }

    @Override
    public void updateTaskSendMessageFail(String userId, String messageId) {
        Task taskReq = new Task() ;
        taskReq.setMessageId(messageId);
        taskReq.setUserId(userId);
        taskDao.updateTaskSendMessageFail(taskReq);
    }
}
