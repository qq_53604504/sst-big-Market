package org.example.infrastructure.persistent.dao;

import org.apache.ibatis.annotations.Mapper;
import org.example.infrastructure.persistent.po.StrategyRule;

import java.util.List;

@Mapper
public interface IStrategyRuleDao {

    List<StrategyRule> queryStrategyRuleList();



    StrategyRule queryStrategyRuleByStrategyIdAndRuleModel(Long strategyId,String ruleModel);

    StrategyRule queryStrategyRuleByStrategyIdAndRuleModelAndAwardId(String strategyId, String ruleModel, String awardId);
}
