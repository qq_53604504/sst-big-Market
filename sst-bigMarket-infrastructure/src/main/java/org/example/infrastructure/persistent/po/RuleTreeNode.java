package org.example.infrastructure.persistent.po;

import lombok.Data;

import java.util.Date;

@Data
public class RuleTreeNode {
    Integer Id;
    String treeId;
    String ruleKey;
    String ruleDesc;
    String ruleValue;
    private Date createTime;
    private Date updateTime;

}
