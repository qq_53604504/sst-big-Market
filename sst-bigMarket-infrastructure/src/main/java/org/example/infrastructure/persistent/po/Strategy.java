package org.example.infrastructure.persistent.po;

import lombok.Data;

import java.util.Date;

/**
 * 策略
 */
@Data
public class Strategy {

    private Integer id;
    private Integer strategyId;
    private String strategyDesc;
    /**
     * 策略规则(抽奖前规则)
     */
    private String ruleModels;
    private Date createTime;
    private Date updateTime;
}
