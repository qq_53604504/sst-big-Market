package org.example.infrastructure.persistent.po;

import lombok.Data;

import java.util.Date;

@Data
public class RuleTree {

    private Integer id;
    private String treeId;
    private String treeName;
    private String treeDesc;
     //树根节点
    private String treeNodeRuleKey;
    private Date createTime;
    private Date updateTime;
}
