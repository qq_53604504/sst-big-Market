package org.example.test.infrastructure;

import lombok.extern.slf4j.Slf4j;
import org.example.infrastructure.persistent.dao.IRaffleActivitySkuDao;
import org.example.infrastructure.persistent.po.RaffleActivitySku;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class RaffleActivitySkuDaoTest {

    @Autowired
    private IRaffleActivitySkuDao IRaffleActivitySkuDao;

    @Test
    public void test_queryRaffleActivateSkuBySku(){
        Long sku =9011L;
        RaffleActivitySku raffleActivitySku = IRaffleActivitySkuDao.queryRaffleActivitySkuBySku(sku);
        log.info(raffleActivitySku.toString());

    }

}
