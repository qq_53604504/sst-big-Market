package org.example.test.domain.strategy;

import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;
import org.example.domain.strategy.model.valobj.*;
import org.example.domain.strategy.service.rule.Tree.Engine.factory.DefaultTreeFactory;
import org.example.domain.strategy.service.rule.Tree.Engine.IDecisionTreeEngine;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashMap;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class LogicTreeTest {

    @Autowired
    private DefaultTreeFactory defaultTreeFactory;

    @Test
    public void test_tree_rule() {
        RuleTreeNodeVO rule_lock = RuleTreeNodeVO
                .builder()
                .nodeKey("rule_lock") //根据这个key来取对应的Node和RuleTreeNodeLineVO用它来判断走向
                .ruleTreeNodeLineVOList(new ArrayList<RuleTreeNodeLineVO>() {{ //匿名内部类的使用,外层{}表示使用匿名内部类,里层{}表示使用匿名内部类的方法和属性

                    add(RuleTreeNodeLineVO.builder()
                            .fromNodeKey("rule_lock")
                            .toNodeKey("rule_stock")
                            .limitType(RuleLimitTypeVO.EQUAL)
                            .limitValue(RuleLimitValueVO.ALLOW)
                            .build());

                    add(RuleTreeNodeLineVO.builder()
                            .fromNodeKey("rule_lock")
                            .toNodeKey("rule_luck")
                            .limitType(RuleLimitTypeVO.EQUAL)
                            .limitValue(RuleLimitValueVO.TAKE_OVER)
                            .build());

                }})
                .build();


        RuleTreeNodeVO rule_stock = RuleTreeNodeVO
                .builder()
                .nodeKey("rule_stock")
                .ruleTreeNodeLineVOList(new ArrayList<RuleTreeNodeLineVO>() {{ //匿名内部类的使用,外层{}表示使用匿名内部类,里层{}表示使用匿名内部类的方法和属性

                    add(RuleTreeNodeLineVO.builder()
                            .fromNodeKey("rule_stock")
                            .toNodeKey("rule_luck")
                            .limitType(RuleLimitTypeVO.EQUAL)
                            .limitValue(RuleLimitValueVO.TAKE_OVER)
                            .build());

                }})
                .build();


        RuleTreeNodeVO rule_luck = RuleTreeNodeVO
                .builder()
                .nodeKey("rule_luck")
                .build();


        RuleTreeVO ruleTreeVO = new RuleTreeVO();
        ruleTreeVO.setRootNodeKey("rule_lock");
        ruleTreeVO.setRuleTreeId(String.valueOf(100000001));
        ruleTreeVO.setRuleTreeDesc("决策树规则；增加dall-e-3画图模型");
        ruleTreeVO.setRuleTreeNodeVOMap(new HashMap<String , RuleTreeNodeVO>(){{
            put("rule_lock",rule_lock);
            put("rule_stock",rule_stock);
            put("rule_luck",rule_luck);
        }});

        IDecisionTreeEngine treeEngine = defaultTreeFactory.openLogicTree(ruleTreeVO);

        DefaultTreeFactory.StrategyAwardVO strategyAwardVO = treeEngine.pocess("xiaofuge",100001L,"100");


        log.info("测试结果：{}", JSON.toJSONString(strategyAwardVO));
    }

}
