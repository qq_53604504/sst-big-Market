package org.example.test.domain.strategy;


import lombok.extern.slf4j.Slf4j;
import org.example.domain.strategy.service.rule.Chain.ILogicChain;
import org.example.domain.strategy.service.rule.Chain.imp.RuleWeightLogicChain;
import org.example.domain.strategy.service.rule.Chain.factory.DefaultLogicChainFactory;
import org.example.domain.strategy.service.armory.IStrategyArmory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * 抽奖责任链测试，验证不同的规则走不同的责任链
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class LogicChainTest {

    @Resource
    private IStrategyArmory strategyArmory;
    @Resource
    private RuleWeightLogicChain ruleWeightLogicChain;
    @Resource
    private DefaultLogicChainFactory defaultLogicChainFactory;

    @Before
    public void setUp() {
        // 策略装配 100001、100002、100003
        log.info("测试结果：{}", strategyArmory.assembleLotteryStrategy(100001L));
        log.info("测试结果：{}", strategyArmory.assembleLotteryStrategy(100002L));
        log.info("测试结果：{}", strategyArmory.assembleLotteryStrategy(100003L));
    }

    @Test
    public void test_LogicChain_rule_blacklist() {
        ILogicChain logicChain = defaultLogicChainFactory.openLogicChain(100001L);
        DefaultLogicChainFactory.StrategyAwardVO chainStrategyAwardVO = logicChain.logic( 100001L,"user001");
        log.info("测试结果：{}", chainStrategyAwardVO.getAwardId());
    }

    @Test
    public void test_LogicChain_rule_weight() {
        // 通过反射 mock 规则中的值
        //ReflectionTestUtils.setField(ruleWeightLogicChain, "userScore", 4900L);

        ILogicChain logicChain = defaultLogicChainFactory.openLogicChain(100001L);
        DefaultLogicChainFactory.StrategyAwardVO chainStrategyAwardVO = logicChain.logic(100001L,"xiaofuge");
        log.info("测试结果：{}", chainStrategyAwardVO.getAwardId());
    }

    @Test
    public void test_LogicChain_rule_default() {
        ILogicChain logicChain = defaultLogicChainFactory.openLogicChain(100001L);
        DefaultLogicChainFactory.StrategyAwardVO chainStrategyAwardVO = logicChain.logic(100001L,"xiaofuge" );
        log.info("测试结果：{}",  chainStrategyAwardVO.getAwardId());
    }

}
