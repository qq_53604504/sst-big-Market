package org.example.test.domain.strategy;

import lombok.extern.slf4j.Slf4j;
import org.example.domain.strategy.service.armory.IStrategyArmory;
import org.example.domain.strategy.service.armory.IStrategyDispatch;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class Strategy {

    @Resource
    private IStrategyDispatch strategyDispatch;

    @Resource
    private IStrategyArmory strategyArmory;
    @Test
    public void test_strategyArmory() {
        boolean success = strategyArmory.assembleLotteryStrategy(100001L);
        log.info("测试结果：{}", success);
    }

    @Test
    public void test_getRandomAwardId() {
        Integer randomAwardId1 = strategyDispatch.getRandomAwardId(100003L);
        Integer randomAwardId2 = strategyDispatch.getRandomAwardId(100003L);
        Integer randomAwardId3 = strategyDispatch.getRandomAwardId(100003L);
        Integer randomAwardId4 = strategyDispatch.getRandomAwardId(100003L);
        Integer randomAwardId5 = strategyDispatch.getRandomAwardId(100003L);

        log.info("测试结果：{}", randomAwardId1);
        log.info("测试结果：{}", randomAwardId2);
        log.info("测试结果：{}", randomAwardId3);
        log.info("测试结果：{}", randomAwardId4);
        log.info("测试结果：{}", randomAwardId5);



    }


    @Test
    public void test_getRandomAwardId_ruleWeightValue() {
        log.info("测试结果：{} - 4000 策略配置", strategyDispatch.getRandomAwardId(100001L, "4000:102,103,104,105"));
        log.info("测试结果：{} - 5000 策略配置", strategyDispatch.getRandomAwardId(100001L, "5000:102,103,104,105,106,107"));
        log.info("测试结果：{} - 6000 策略配置", strategyDispatch.getRandomAwardId(100001L, "6000:102,103,104,105,106,107,108,109"));

    }
}
