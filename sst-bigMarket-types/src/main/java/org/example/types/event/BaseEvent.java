package org.example.types.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 抽象基础事件
 * 用于提取每个消息的共性:
 * 1.Topic
 * 2.发送的消息格式(消息id,时间戳,消息内容),通过内部类实现,因为只在这里面用到,其他用不到
 * 3.构建消息的方法
 */
@Data
public abstract class BaseEvent<T> {

    public abstract EventMessage<T> buildEventMessage(T data);

    public abstract String topic();

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class EventMessage<T> {
        private String id;
        private Date timestamp;
        private T data;
    }

}
