package org.example.types.common;

import java.text.SimpleDateFormat;

public class Constants {

    public static final SimpleDateFormat dateFormatMonth = new SimpleDateFormat("yyyy-MM");
    public static final SimpleDateFormat dateFormatDay = new SimpleDateFormat("yyyy-MM-dd");

    public final static String SPLIT = ",";
    public final static String COLON = ":";
    public final static String SPACE = " ";
    public final static String UNDERLINE = "_";

    public static class RedisKey {
        public static String ACTIVITY_KEY = "sst:big_market_activity_key_";
        public static String ACTIVITY_SKU_KEY = "sst:big_market_activity_sku_key_";
        public static String ACTIVITY_COUNT_KEY = "sst:big_market_activity_count_key_";
        public static String STRATEGY_KEY = "sst:strategy:big_market_strategy_key_";
        public static String STRATEGY_AWARD_KEY = "sst:big_market_strategy_award_key_";
        public static String STRATEGY_AWARD_LIST_KEY = "sst:big_market_strategy_award_list_key_";
        public static String STRATEGY_RATE_TABLE_KEY = "sst:rate_table:big_market_strategy_rate_table_key_";
        public static String STRATEGY_RATE_RANGE_KEY = "sst:rate_range:big_market_strategy_rate_range_key_";
        public static String RULE_TREE_VO_KEY = "sst:rule_tree_vo_key_";
        public static String STRATEGY_AWARD_COUNT_KEY = "sst:award_count:strategy_award_count_key_";
        //mysql中award减库存队列消息
        public static String STRATEGY_AWARD_COUNT_QUERY_KEY = "sst:queue:strategy_award_count_subtraction_queue_key:";
        //mysql中sku减库存队列消息
        public static String ACTIVITY_SKU_COUNT_QUERY_KEY = "sst:queue:activity_sku_count_subtraction_queue_key:";
        public static String ACTIVITY_SKU_STOCK_COUNT_KEY = "sst:stock_lock:activity_sku_stock_count_key_";

        /**
         * 积分账户分布式锁
         */
        public static String USER_CREDIT_ACCOUNT_LOCK = "user_credit_account_lock_";

        /**
         * 活动额度账户分布式锁
         */
        public static String ACTIVITY_ACCOUNT_LOCK = "activity_account_lock_";

        /**
         * 活动额度账户更新分布式锁
         */
        public static String ACTIVITY_ACCOUNT_UPDATE_LOCK = "activity_account_update_lock_";

    }

}
